FROM adoptopenjdk/openjdk14:alpine-jre
EXPOSE 9000
ADD build/libs/bulletinEvents-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
