package com.netcracker.data.configs;

import com.netcracker.data.security.FIlterForJwt;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import javax.servlet.http.HttpServletResponse;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.DELETE;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final FIlterForJwt fIlterForJwt;

  public SecurityConfig(FIlterForJwt fIlterForJwt) {
    this.fIlterForJwt = fIlterForJwt;
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder(){
    return new BCryptPasswordEncoder();
  }

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception{
    httpSecurity
            .httpBasic().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .authorizeRequests()
            .antMatchers(GET, "/event/*").permitAll()
            .antMatchers(GET, "/event/all").permitAll()
            .antMatchers(GET, "/event/old").permitAll()
            .antMatchers(GET, "/event/").permitAll()
            .antMatchers(GET, "/event/search").permitAll()
            .antMatchers(POST, "/event/").authenticated()
            .antMatchers(PUT, "/event/*").authenticated()
            .antMatchers(DELETE, "/event/*").authenticated()
            .antMatchers(PATCH, "/event/*").authenticated()
            .antMatchers(PATCH, "/event/*/comment").authenticated()
            .antMatchers(PATCH, "/event/*/favorites").authenticated()
            .antMatchers(PATCH, "/event/*/like").authenticated()
            .antMatchers(PATCH, "/event/*/rating").authenticated()
            .antMatchers(PUT, "/event/*/delete/comment").authenticated()
            .antMatchers(PUT, "/event/*/delete/favorites").authenticated()
            .antMatchers(PUT, "/event/*/delete/like").authenticated()
            .antMatchers(PUT, "/event/*/delete/user").authenticated()
            .antMatchers(GET, "/event/recommendation/*").authenticated()
            .antMatchers(POST, "/event/updateAllRecommendation/").authenticated()
            .antMatchers(GET, "/user/*").authenticated()
            .antMatchers(GET, "/user/").authenticated()
            .antMatchers(POST, "/user/").anonymous()
            .antMatchers(PATCH, "/user/*").authenticated()
            .antMatchers(DELETE, "/user/").authenticated()
            .antMatchers(PUT, "/user/").authenticated()
            .antMatchers(GET, "/user/*/events").authenticated()
            .antMatchers(PATCH, "/user/*/categories").authenticated()
//                .antMatchers(PATCH, "/user/*/categories").authenticated()
            .antMatchers(GET, "/user/*/categories").authenticated()
            .antMatchers(GET, "/user/*/favourites").authenticated()
            .antMatchers(GET, "/user/*/countCreateEvent").authenticated()
            .antMatchers(POST, "/user/*/premium").authenticated()
            .antMatchers(POST, "/security/*").anonymous()
            .antMatchers(GET, "/countries/*").permitAll()
            .antMatchers(POST, "/countries/").authenticated()
            .antMatchers(PUT, "/countries/*").authenticated()
            .antMatchers(DELETE, "/countries/*").authenticated()
            .antMatchers(GET, "/countries/").permitAll()
            .antMatchers(GET, "/countries/cities").permitAll()
            // Добавить новые методы
            .and()
            .addFilterBefore(fIlterForJwt, UsernamePasswordAuthenticationFilter.class)
            .logout()
            .logoutUrl("/security/logout")
            .permitAll()
            .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> httpServletResponse.setStatus(HttpServletResponse.SC_OK));
  }
}
