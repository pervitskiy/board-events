package com.netcracker.data.configs;

import com.netcracker.data.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class SpringScheduledConfig {

  @Autowired
  private EventService eventService;

  @Scheduled(fixedDelay = 60000)
  public void scheduleFixedDelayTask() {
    System.out.println("Обновление питона старт");
    eventService.updateAllRecommendation();
    System.out.println("Обновление питона финиш");
  }
}
