package com.netcracker.data.criteria;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@Builder
public class EventSearchCriteria {
	private List<Integer> categories;
	private List<String> categoriesName;
	private Optional<Date> dateLaterThan;
	private Optional<Date> dateEarlierThan;
	private Optional<Integer> cityUuid;
	private Optional<Integer> countryUuid;
	private Optional<Double> ratingHigherThan;
	private Optional<Double> ratingLowerThan;
	private Optional<Integer> membersHigherThan;
	private Optional<Integer> membersLowerThan;
	private Optional<String> query;
}
