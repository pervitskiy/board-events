package com.netcracker.data.criteria;

import com.netcracker.data.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class EventSpecifications {

	static final Logger logger = LoggerFactory.getLogger(EventSpecifications.class);

//	static Predicate descriptionPred;

	public static Specification<Event> createEventSpecifications(EventSearchCriteria criteria) {
		return searchQuery(criteria.getQuery())
						.and(cityEqualTo(criteria.getCityUuid()))
						.and(dateLaterThan(criteria.getDateLaterThan()))
						.and(dateEarlierThan(criteria.getDateEarlierThan()))
						.and(countryEqualTo(criteria.getCountryUuid()))
						.and(membersCountBetween(criteria.getMembersHigherThan(), criteria.getMembersLowerThan()))
						.and(ratingBetween(criteria.getRatingHigherThan(), criteria.getRatingLowerThan()))
						.and(categoryIn(criteria.getCategories()));
	}

	public static Specification<Event> cityEqualTo(Optional<Integer> cityUuid) {
		logger.debug(String.valueOf(cityUuid));
		return cityUuid.<Specification<Event>>map(integer -> (root, query, criteriaBuilder) -> {
			Join<Event, City> cityJoin = root.join(Event_.city);
			return cityJoin.get(City_.cityId).in(integer);
		}).orElse(null);
	}

	public static Specification<Event> dateEarlierThan(Optional<Date> date) {
		return (root, query, criteriaBuilder) -> {
			return date.map(currentDate -> criteriaBuilder.lessThanOrEqualTo(root.get("dateTime").as(Date.class), date.get()))
							.orElse(null);
		};
	}

	public static Specification<Event> dateLaterThan(Optional<Date> date) {
		return (root, query, criteriaBuilder) -> {
			return date.map(currentDate -> criteriaBuilder.greaterThanOrEqualTo(root.get("dateTime").as(Date.class), date.get()))
							.orElse(null);
		};
	}

//	public static Specification<Event> membersMoreThan(Optional<Integer> members) {
//		return (root, query, criteriaBuilder) -> {
//			return members.map(count -> criteriaBuilder.greaterThanOrEqualTo(root.get("membersCount").as(Integer.class), members.get()))
//							.orElse(null);
//		};
//	}

	public static Specification<Event> membersCountBetween(Optional<Integer> minRate, Optional<Integer> maxRate) {
		return (root, query, builder) -> {
			return minRate.map(min -> {
				return maxRate.map(max -> builder.between(root.get(Event_.membersCount), min, max)
				).orElse(null);
			}).orElse(null);
		};
	}

	public static Specification<Event> ratingBetween(Optional<Double> minRate, Optional<Double> maxRate) {
		return (root, query, builder) -> {
			return minRate.map(min -> {
				return maxRate.map(max -> builder.between(root.get(Event_.rating), min, max)
				).orElse(null);
			}).orElse(null);
		};
	}

	public static Specification<Event> countryEqualTo(Optional<Integer> countryUuid) {
		logger.debug(String.valueOf("country uuid is " + countryUuid));
		return countryUuid.<Specification<Event>>map(integer -> (root, query, criteriaBuilder) -> {
			Join<Event, City> cityJoin = root.join(Event_.city);
			Join<City, Country> countryJoin = cityJoin.join(City_.country);
			//event -> city -> country
			return countryJoin.get(Country_.countryId).in(integer);
		}).orElse(null);
	}

	public static Specification<Event> categoryIn(List<Integer> categories) {
		if (CollectionUtils.isEmpty(categories)) {
			return null;
		}
		return (root, query, builder) -> {
			//event -> eventhascategories -> categoriesEvents
			Join<Event, EventsHasCategories> eventsHasCategoriesJoin = root.join(Event_.eventsCategories);
			Join<EventsHasCategories, CategoriesEvents> categoryJoin = eventsHasCategoriesJoin.join(EventsHasCategories_.category);
			return categoryJoin.get(CategoriesEvents_.categoryEventID).in(categories);
		};
	}

	public static Specification<Event> searchQuery(Optional<String> searchQuery) {
		logger.debug(searchQuery.get());
		return (root, query, criteriaBuilder) -> {
			List<String> queryList = new ArrayList<>();
//			if (searchQuery.get().contains(" ")) {
//				System.out.println("contains space");
			queryList = Arrays.asList(searchQuery.get().split("\\s+"));
//			}
			logger.debug(String.valueOf(queryList));

			List<Predicate> allPredicates = new ArrayList<>();
			List<Predicate> briefPredicates = new ArrayList<>();
			List<Predicate> fullPredicates = new ArrayList<>();
			Predicate descriptionPred = criteriaBuilder.or(
							criteriaBuilder.and(briefPredicates.toArray(Predicate[]::new)),
							criteriaBuilder.and(fullPredicates.toArray(Predicate[]::new)),
							criteriaBuilder.and(allPredicates.toArray(Predicate[]::new))
			);

			if (!queryList.isEmpty()) {
				ArrayList<String> modified = new ArrayList<>();
				for (String str : queryList) {
					modified.add("%" + str.toLowerCase(Locale.ROOT) + "%");
				}
				queryList = modified;
				logger.debug(String.valueOf(queryList));

				for (String str : queryList) {
					briefPredicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("briefDescription")), str));
					fullPredicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("fullDescription")), str));
				}

				allPredicates = Stream.of(briefPredicates, fullPredicates)
								.flatMap(Collection::stream)
								.collect(Collectors.toList());

				descriptionPred = criteriaBuilder.or(
								criteriaBuilder.and(briefPredicates.toArray(Predicate[]::new)),
								criteriaBuilder.and(fullPredicates.toArray(Predicate[]::new)),
								criteriaBuilder.and(allPredicates.toArray(Predicate[]::new))
				);
			}
			return criteriaBuilder.and(descriptionPred);
		};
	}
}
