package com.netcracker.data.criteria.service;

import com.netcracker.data.criteria.EventSearchCriteria;
import com.netcracker.data.models.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface EventCriteriaService {

	Page<Event> retrieveEvents(EventSearchCriteria criteria, Pageable pageable);

	Optional<Event> retrieveEvent(UUID uuid);
}
