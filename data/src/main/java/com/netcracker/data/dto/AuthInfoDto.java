package com.netcracker.data.dto;

import java.util.Objects;
import java.util.UUID;

public class AuthInfoDto {
    private String token;
    private UUID id;

  public AuthInfoDto(String token, UUID id) {
    this.token = token;
    this.id = id;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthInfoDto authInfo = (AuthInfoDto) o;
        return Objects.equals(token, authInfo.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }
}
