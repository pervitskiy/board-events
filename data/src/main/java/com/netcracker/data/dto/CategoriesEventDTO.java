package com.netcracker.data.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CategoriesEventDTO {
    private Integer categoryEventID;
    private String nameCategory;
    private String color;
}
