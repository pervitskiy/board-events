package com.netcracker.data.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CategoriesMediaDTO {
    private Integer categoryFileId;
    private String categoryFileName;
}
