package com.netcracker.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.netcracker.data.models.Country;
import com.netcracker.data.models.Region;
import com.netcracker.data.transfer.Exist;
import com.netcracker.data.transfer.New;
import lombok.Data;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityDTO {
    @NotNull(groups = {Exist.class})
    private UUID cityUuidId;
    @NotNull(groups = {Exist.class})
    private String areaRu;
    @NotNull(groups = {Exist.class})
    private String titleRu;
    @NotNull(groups = {Exist.class})
    private String regionRu;
    @NotNull(groups = {Exist.class})
    private Boolean important;

    @NotNull(groups = {Exist.class})
    private UUID countryId;
    private String country;
    @NotNull(groups = {Exist.class})
    private UUID regionId;
    private String region;


}
