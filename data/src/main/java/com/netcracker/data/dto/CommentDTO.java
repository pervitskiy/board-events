package com.netcracker.data.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class CommentDTO {
    private UUID commentId;
    private String message;
    private LocalDateTime commentDate;
    private UserDTO user;
}
