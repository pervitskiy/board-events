package com.netcracker.data.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.netcracker.data.transfer.AdminDetails;
import com.netcracker.data.transfer.Details;
import com.netcracker.data.transfer.New;
import com.netcracker.data.transfer.UpdateName;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.UUID;

@Data
public class CountryDTO {
    @JsonView({Details.class, AdminDetails.class})
    @Null(groups = {New.class, UpdateName.class})
    private UUID countryUuidId;

    @JsonView({Details.class, AdminDetails.class})
    @NotNull(groups = {New.class, UpdateName.class})
    private String countryName;

    @JsonView({AdminDetails.class})
    @Null(groups = {New.class, UpdateName.class})
    private Integer countryId;
}
