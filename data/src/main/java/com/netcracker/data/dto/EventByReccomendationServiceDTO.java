package com.netcracker.data.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class EventByReccomendationServiceDTO implements Serializable {
  private UUID _id;
  private String date_time;
  private String place;
  private String brief_description;
  private String full_description;
}
