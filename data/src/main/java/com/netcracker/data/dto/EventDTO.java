package com.netcracker.data.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.netcracker.data.models.*;
import com.netcracker.data.transfer.AdminDetails;
import com.netcracker.data.transfer.Details;
import com.netcracker.data.transfer.New;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Null;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

@Data
public class EventDTO {
	@JsonView({Details.class, AdminDetails.class})
	@Null(groups = New.class)
	private UUID eventId;
  private String ageRestriction;
  private String securityUrl;
	@JsonView({Details.class, AdminDetails.class})
	private Date dateTime;
	@JsonView({Details.class, AdminDetails.class})
	private String place;
	@JsonView({Details.class, AdminDetails.class})
	private String briefDescription;
	@JsonView({Details.class, AdminDetails.class})
	private String fullDescription;
	@JsonView({Details.class, AdminDetails.class})
	private String externalEvent;
	private Double rating;
	private Integer membersCount;
	@JsonView({Details.class, AdminDetails.class})
	private CityDTO cityDTO;
	@JsonView({Details.class, AdminDetails.class})
	private List<UserDTO> usersDTO;
	private List<UserDTO> likesUser;
	private List<UserDTO> favoritesUser;
	private List<MediaDTO> medias;
	@JsonView({Details.class, AdminDetails.class})
	private List<UserDTO> organizersDTO;
	private List<CategoriesEventDTO> categoriesEventDTOS;
	private List<CommentDTO> comments;
	private List<RatingDTO> ratings;
	private Date creation_date;
}
