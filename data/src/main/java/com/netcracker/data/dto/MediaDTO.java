package com.netcracker.data.dto;

import lombok.Data;

import javax.persistence.Column;
import java.util.UUID;

@Data
public class MediaDTO {
    private UUID mediaUuid;
    private String mediaUrl;
    private CategoriesMediaDTO categories;
    private Integer width;
    private Integer height;
    private String securityUrl;
}
