package com.netcracker.data.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PaymentInformationDTO {
  private UUID uuid;
  private Date createDate;
  private String status;
  private String emailAddress;
  private String value;
}
