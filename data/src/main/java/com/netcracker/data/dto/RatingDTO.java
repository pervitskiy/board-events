package com.netcracker.data.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class RatingDTO {
	private UUID ratingId;
	private float valRating;
	private UserDTO user;
}
