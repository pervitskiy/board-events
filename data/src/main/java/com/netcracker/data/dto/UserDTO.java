package com.netcracker.data.dto;
import com.fasterxml.jackson.annotation.JsonView;
import com.netcracker.data.models.Event;
import com.netcracker.data.transfer.AdminDetails;
import com.netcracker.data.transfer.Details;
import com.netcracker.data.transfer.New;
import com.netcracker.data.transfer.PatchMedia;
import lombok.Data;
import org.hibernate.annotations.OrderBy;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
//@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {

    @Null(groups = {New.class, PatchMedia.class})
    private UUID userId;

    @JsonView({Details.class, AdminDetails.class})
    @NotNull(groups = {New.class})
    @Null(groups = {PatchMedia.class})
    private String firstName;

    @JsonView({Details.class, AdminDetails.class})
    @NotNull(groups = {New.class})
    @Null(groups = {PatchMedia.class})
    private String lastName;

    private String informationAboutYourself;
    private Boolean prem;
    private Date lastDatePrem;

    private String username;

    @NotNull(groups = {New.class})
    @Email(groups = {New.class})
    @Null(groups = {PatchMedia.class})
    private String email;

    @Null(groups = {PatchMedia.class})
    @NotNull(groups = {New.class})
    private String password;

    @NotNull(groups = {PatchMedia.class})
    private List<MediaDTO> medias;

    private List<CategoriesEventDTO> categories;

    private CountryDTO country;

    private CityDTO cityDTO;

    //private List<Event> events;

    public void addMediaList(List<MediaDTO> mediaDTOList){
        medias.addAll(mediaDTOList);
    }
}
