package com.netcracker.data.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

@Data
public class UserEventsDTO implements Serializable {
	private UUID eventId;
	private String dateTime;
	private String place;
	private String briefDescription;
	private String fullDescription;
}
