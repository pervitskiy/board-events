package com.netcracker.data.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserEventsItemsDTO {
  private List<EventByReccomendationServiceDTO> eventsDTOList;

  public UserEventsItemsDTO() {
    eventsDTOList = new ArrayList<>();
  }
  public UserEventsItemsDTO(List<EventByReccomendationServiceDTO> eventsDTOList) {
    this.eventsDTOList = eventsDTOList;
  }
}
