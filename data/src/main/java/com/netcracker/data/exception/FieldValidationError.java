package com.netcracker.data.exception;

import lombok.Data;

@Data
public class FieldValidationError {
    private String object;
    private String field;
    private Object rejectedValue;
    private String message;

    public FieldValidationError() {
    }

    public FieldValidationError(String object, String message) {
        this.object = object;
        this.message = message;
    }
}