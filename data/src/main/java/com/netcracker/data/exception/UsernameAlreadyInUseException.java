package com.netcracker.data.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UsernameAlreadyInUseException extends Exception{
  private String username;

  public UsernameAlreadyInUseException(String username) {
    super();
    this.username = username;
  }

  public UsernameAlreadyInUseException(String message, String username) {
    super(message);
    this.username = username;
  }
}
