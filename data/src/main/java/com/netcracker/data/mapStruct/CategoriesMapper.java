package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.CategoriesEventDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.*;
import com.netcracker.data.repository.CategoryEventsRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel="spring")
public abstract class CategoriesMapper {
  @Autowired
  public CategoryEventsRepository categoryEventsRepository;

  @Mappings({
    @Mapping(target="categoryEventID", source="categoriesHasUsers.categoriesEvents.categoryEventID"),
    @Mapping(target="nameCategory", source="categoriesHasUsers.categoriesEvents.nameCategory"),
    @Mapping(target="color", source="categoriesHasUsers.categoriesEvents.color")

  })
  public abstract CategoriesEventDTO toCategoriesDTO (CategoriesHasUsers categoriesHasUsers);
  public abstract List<CategoriesEventDTO> toCategoriesDTOs (List<CategoriesHasUsers> categoriesHasUsers);

  public abstract List<CategoriesEventDTO> toCategoriesDTOsFromCategories (List<CategoriesEvents> categoriesEvents);


  @Mappings({
    @Mapping(target="categoryEventID", source="eventsHasCategories.category.categoryEventID"),
    @Mapping(target="nameCategory", source="eventsHasCategories.category.nameCategory"),
    @Mapping(target="color", source="eventsHasCategories.category.color")

  })
  public abstract CategoriesEventDTO toCategoriesDTO (EventsHasCategories eventsHasCategories);
  public abstract List<CategoriesEventDTO> toCategoriesDTOsFromEventsHasCategories (List<EventsHasCategories> eventsHasCategories);

  public CategoriesHasUsers toCategoriesHasUsers(CategoriesEventDTO categoriesEventDTO) throws EntityNotFoundException {
    if (categoriesEventDTO != null) {
      CategoriesHasUsers categoriesHasUsers = new CategoriesHasUsers();
      categoryEventsRepository.findByNameCategory(categoriesEventDTO.getNameCategory()).
        orElseThrow(() -> new EntityNotFoundException(categoriesEventDTO.getCategoryEventID(), "CategoriesEvents")).
        addCategoryHasUsers(categoriesHasUsers);
      return categoriesHasUsers;
    } else return null;
  }

  public EventsHasCategories toEventsHasCategories(CategoriesEventDTO categoriesEventDTO) throws EntityNotFoundException {
      if (categoriesEventDTO != null){
          EventsHasCategories eventsHasCategories = new EventsHasCategories();
          categoryEventsRepository.findByNameCategory(categoriesEventDTO.getNameCategory()).
                  orElseThrow(() -> new EntityNotFoundException(categoriesEventDTO.getCategoryEventID(), "CategoriesEvents")).
                  addEventsHasCategories(eventsHasCategories);
          return eventsHasCategories;
      } else return null;
  }
}
