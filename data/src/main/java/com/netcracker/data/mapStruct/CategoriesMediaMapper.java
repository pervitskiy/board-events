package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.CategoriesMediaDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.CategoryFile;
import com.netcracker.data.repository.CategoryFileRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel="spring")
public abstract class CategoriesMediaMapper {
    @Autowired
    private CategoryFileRepository categoryFileRepository;

    public abstract CategoriesMediaDTO toCategoriesMediaDTo(CategoryFile categoryFile);
    public abstract List<CategoriesMediaDTO> toCategoriesMediaDTos(List<CategoryFile> categoryFile);
    public CategoryFile findCategoryFileById(CategoriesMediaDTO categoriesMediaDTO) throws EntityNotFoundException {
        if (categoriesMediaDTO != null) {
            int id = categoriesMediaDTO.getCategoryFileId();
            return categoryFileRepository.findById(id).
                    orElseThrow(() -> new EntityNotFoundException(id, "Category File"));
        }else return null;
    }
}
