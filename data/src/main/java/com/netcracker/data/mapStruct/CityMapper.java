package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.CityDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.City;
import com.netcracker.data.repository.CityRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel="spring", uses = CountryMapper.class)
public abstract class CityMapper {
    @Autowired
    private CityRepository cityRepository;
    @Mappings({
            @Mapping(target="countryId", source="city.country.countryUuidId"),
            @Mapping(target="regionId", source="city.region.regionUuidId"),
            @Mapping(target="country", source="city.country.titleRu"),
            @Mapping(target="region", source="city.region.titleRu")
    })
    public abstract CityDTO toCityDTO (City city);
    public  abstract List<CityDTO> toCityDTOs(List<City> city);

    public City toCityByCityDTOId(CityDTO cityDTO) throws EntityNotFoundException {
        if (cityDTO != null) {
          return cityRepository.findById(cityDTO.getCityUuidId()).
            orElseThrow(()-> new EntityNotFoundException(cityDTO.getCityUuidId(),"City"));
        }
        else return null;
    }
}
