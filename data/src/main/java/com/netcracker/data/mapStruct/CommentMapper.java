package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.CommentDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.Comment;
import com.netcracker.data.repository.UserRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, EventMapper.class})
public abstract class CommentMapper {
	@Autowired
	private UserRepository userRepository;

	public abstract CommentDTO toCommentDTO(Comment comment);

	public abstract List<CommentDTO> toCommentDTOs(List<Comment> comments);

	public Comment toComment(CommentDTO commentDTO) throws EntityNotFoundException {
		if (commentDTO == null) {
			return null;
		}
		Comment comment = new Comment();

		comment.setUser(userRepository.findById(commentDTO.getUser().getUserId())
						.orElseThrow(() -> new EntityNotFoundException(commentDTO.getUser().getUserId(), "User")));
		comment.setCommentId(commentDTO.getCommentId());
		comment.setMessage(commentDTO.getMessage());

		return comment;
	}


}
