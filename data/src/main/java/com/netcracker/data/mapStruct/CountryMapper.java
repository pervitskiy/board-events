package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.CountryDTO;
import com.netcracker.data.models.Country;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface CountryMapper {
    @Mappings({
            @Mapping(target="countryName", source="country.titleRu")
    })
    CountryDTO toCountryDTO(Country country);
    List<CountryDTO> toCountryDTOs(List<Country> country);

   // @BeanMapping(ignoreByDefault = true) --поведения по умолчанию будет явное сопоставление .
   // что означает, что все сопоставления должны быть указаны с помощью,
   // @Mappingи при отсутствии целевых свойств предупреждений не будет.
    @Mappings({
            @Mapping(target="countryUuidId", source="countryDto.countryUuidId", defaultExpression = "java(java.util.UUID.randomUUID())"),
            @Mapping(target="titleRu", source="countryDto.countryName"),
            //не работает почему-то автогенерация в бд, поэтому тут временно костыль
            @Mapping(target ="countryId",source = "countryDto.countryId", defaultExpression = "java(new java.util.Random().nextInt(10000-10)+10000)")
    })
    Country toCountry(CountryDTO countryDto);
}
