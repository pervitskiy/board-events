package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.EventDTO;
import com.netcracker.data.dto.UserEventsDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.Event;
import com.netcracker.data.models.UserHasEvents;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CityMapper.class, UserMapper.class, CategoriesMapper.class, MediaMapper.class, CommentMapper.class, RatingMapper.class})
public abstract class EventMapper {
	@Autowired
	private CityMapper cityMapper;

	@Autowired
	private UserMapper userMapper;

	@Mappings({
					@Mapping(target = "usersDTO", source = "event.users"),
					@Mapping(target = "organizersDTO", source = "event.organizers"),
					@Mapping(target = "likesUser", source = "event.likeUserOnEvents"),
					@Mapping(target = "favoritesUser", source = "event.favoritesUserOnEvents"),
					@Mapping(target = "medias", source = "event.media"),
					@Mapping(target = "categoriesEventDTOS", source = "event.eventsCategories"),
					@Mapping(target = "cityDTO", source = "event.city"),
					@Mapping(target = "creation_date", source = "creation_date")
	})
	public abstract EventDTO toEventDTO(Event event);

  public abstract UserEventsDTO toUserEventsDTO(Event event);
  public abstract List<UserEventsDTO> toUserEventsDTOs(List<Event> events);


  @Mappings({
					@Mapping(target = "users", source = "usersDTO"),
					@Mapping(target = "organizers", source = "organizersDTO"),
					@Mapping(target = "media", source = "medias"),
					@Mapping(target = "likeUserOnEvents", source = "likesUser"),
					@Mapping(target = "favoritesUserOnEvents", source = "favoritesUser"),
					@Mapping(target = "eventsCategories", source = "categoriesEventDTOS"),
					@Mapping(target = "city", source = "cityDTO"),
					@Mapping(target = "creation_date", ignore = true)
	})
	public abstract Event toEvent(EventDTO eventDTO) throws EntityNotFoundException;

	public abstract List<EventDTO> toEventDTOs(List<Event> events);

	//возможно пропадут лайки)
	@Mappings({
					@Mapping(target = "eventId", ignore = true),
					@Mapping(target = "users", source = "usersDTO"),
					@Mapping(target = "organizers", source = "organizersDTO"),
					@Mapping(target = "media", source = "medias"),
					@Mapping(target = "eventsCategories", source = "categoriesEventDTOS"),
					@Mapping(target = "city", source = "cityDTO"),
					@Mapping(target = "rating", source = "rating"),
					@Mapping(target = "membersCount", source = "membersCount"),
	})
	public abstract Event updateEventFromEventDto(EventDTO eventDTO, @MappingTarget Event event) throws EntityNotFoundException;

	public Event patchEventFromEventDto(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		Event newEvent = toEvent(eventDTO);
		event.addUsers(newEvent.getUsers());
//		event.addToMembersCount();
		event.setMembersCount(event.getMembersCount()+1);
//    event.addOrganizers(newEvent.getOrganizers());
//    event.addMedias(newEvent.getMedia());
//    event.addEventsCategories(newEvent.getEventsCategories());
//    event.setCity( cityMapper.toCityByCityDTOId( eventDTO.getCityDTO() ) );
//    event.addRatings( newEvent.getRatings());
//    event.addComments(newEvent.getComments());
//
//    event.setDateTime( newEvent.getDateTime() );
//    event.setPlace( newEvent.getPlace() );
//    event.setBriefDescription( newEvent.getBriefDescription() );
//    event.setFullDescription( newEvent.getFullDescription() );
//    event.setExternalEvent( newEvent.getExternalEvent() );
		return event;
	}

	public Event patchEventCommentFromEventDto(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		Event newEvent = toEvent(eventDTO);
		event.addComments(newEvent.getComments());
		return event;
	}

	public Event patchEventRatingFromEventDto(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		Event newEvent = toEvent(eventDTO);
		event.addRatings(newEvent.getRatings());
		return event;
	}

	public Event patchEventLikeFromEventDto(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		Event newEvent = toEvent(eventDTO);
		event.addLikeUserOnEvents(newEvent.getLikeUserOnEvents());
		return event;
	}

	public Event patchEventFavoritesFromEventDto(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		Event newEvent = toEvent(eventDTO);
		event.addFavoritesUserOnEvents(newEvent.getFavoritesUserOnEvents());
		return event;
	}
}
