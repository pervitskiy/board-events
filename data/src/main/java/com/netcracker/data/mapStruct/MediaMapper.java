package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.MediaDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.Media;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring", uses = CategoriesMediaMapper.class)
public abstract class MediaMapper {
    @Mappings({
            @Mapping(target="mediaUuid", source="media.mediaID"),
            @Mapping(target="mediaUrl", source="media.fileName"),
            @Mapping(target="categories", source="media.categoriesFile")

    })
    public abstract MediaDTO toMediaDTO(Media media);
    public abstract List<MediaDTO> toMediaDTOs(List<Media> media);
    @Mappings({
            @Mapping(target="mediaID", source="mediaUuid"),
            @Mapping(target="fileName", source="mediaUrl"),
            @Mapping(target="categoriesFile", source="categories")
    })
    public abstract Media toMedia(MediaDTO mediaDTO) throws EntityNotFoundException;

    @Mappings({
            @Mapping(target="mediaID", source="mediaUuid"),
            @Mapping(target="fileName", source="mediaUrl"),
            @Mapping(target="categoriesFile", source="categories")
    })
    public abstract void updateMediaFromDto(MediaDTO mediaDTO, @MappingTarget Media media);
}
