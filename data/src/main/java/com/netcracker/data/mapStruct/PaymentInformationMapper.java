package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.PaymentInformationDTO;
import com.netcracker.data.models.PaymentInformation;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public abstract class PaymentInformationMapper {
   public abstract PaymentInformation toPaymentInformation(PaymentInformationDTO paymentInformationDTO);
   public abstract PaymentInformationDTO toPaymentInformationDTO(PaymentInformation paymentInformation);
}
