package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.RatingDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.Rating;
import com.netcracker.data.repository.UserRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel="spring", uses = UserMapper.class)
public abstract class RatingMapper {
    @Autowired
    private UserRepository userRepository;

    public abstract RatingDTO toRatingDTO(Rating rating);
    public abstract List<RatingDTO> toRatingDTOs(List<Rating> ratings);

    public Rating toRating(RatingDTO ratingDTO) throws EntityNotFoundException {
        if ( ratingDTO == null ) {
            return null;
        }
        Rating rating = new Rating();

        rating.setUser(userRepository.findById(ratingDTO.getUser().getUserId())
                .orElseThrow(() -> new EntityNotFoundException(ratingDTO.getUser().getUserId(), "User")));
        rating.setValRating( ratingDTO.getValRating() );
        rating.setRatingId(ratingDTO.getRatingId() );

        return rating;
    }
}
