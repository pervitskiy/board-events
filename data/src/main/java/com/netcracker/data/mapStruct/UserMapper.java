package com.netcracker.data.mapStruct;

import com.netcracker.data.dto.UserDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.*;
import com.netcracker.data.repository.UserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel="spring", uses = {MediaMapper.class, CategoriesMapper.class, CountryMapper.class, CityMapper.class})
public abstract class UserMapper {
  @Autowired
  private MediaMapper mediaMapper;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CityMapper cityMapper;

  @Autowired
  private CategoriesMapper categoriesMapper;

  @Mappings({
    @Mapping(target = "userId", source="userID"),
    @Mapping(target = "categories", source = "categoriesHasUsers"),
    @Mapping(target = "country", source="user.country"),
    @Mapping(target="cityDTO", source="user.city")

  })
  public abstract UserDTO toUserDTO(User user);

  public UserDTO toUserDTOFromUserHasEvents(UserHasEvents userHasEvents){
    return toUserDTO(userHasEvents.getUser());
  }

  public UserDTO toUserDTOFromLikeUserOnEvent(LikeUserOnEvent likeUserOnEvent){
    return toUserDTO(likeUserOnEvent.getUser());
  }

  public UserDTO toUserDTOFromOrganizersHasEvents(OrganizersHasEvents userHasEvents){
    return toUserDTO(userHasEvents.getOrganizers());
  }

  public UserDTO toUserDTOFromFavoritesUserOnEvents(FavoritesUserOnEvent favoritesUserOnEvent){
    return toUserDTO(favoritesUserOnEvent.getUser());
  }

  public OrganizersHasEvents toOrganizersHasEvents(UserDTO userDTO) throws EntityNotFoundException {
    OrganizersHasEvents organizersHasEvents = new OrganizersHasEvents();
    userRepository.findById(userDTO.getUserId()).
      orElseThrow(() -> new EntityNotFoundException(userDTO.getUserId(), "User"))
      .addOrganizersHasEvents(organizersHasEvents);
    return organizersHasEvents;
  }

  public LikeUserOnEvent toLikeUserOnEvent(UserDTO userDTO) throws EntityNotFoundException {
    LikeUserOnEvent likeUserOnEvent = new LikeUserOnEvent();
    userRepository.findById(userDTO.getUserId()).
      orElseThrow(() -> new EntityNotFoundException(userDTO.getUserId(), "User"))
      .addLikeUserOnEvents(likeUserOnEvent);
    return likeUserOnEvent;
  }

  public FavoritesUserOnEvent toFavoritesUserOnEvents(UserDTO userDTO) throws EntityNotFoundException {
    FavoritesUserOnEvent favoritesUserOnEvent = new FavoritesUserOnEvent();
    userRepository.findById(userDTO.getUserId()).
      orElseThrow(() -> new EntityNotFoundException(userDTO.getUserId(), "User"))
      .addFavoritesUserOnEvent(favoritesUserOnEvent);
    return favoritesUserOnEvent;
  }

  public UserHasEvents toUserHasEvents(UserDTO userDTO) throws EntityNotFoundException {
    UserHasEvents userHasEvents = new UserHasEvents();
    userRepository.findById(userDTO.getUserId()).
      orElseThrow(() -> new EntityNotFoundException(userDTO.getUserId(), "User"))
      .addUserHasEvents(userHasEvents);
    return userHasEvents;
  }

  public abstract List<UserDTO> toUserDTOs(List<User> users);

  @Mappings({
    @Mapping(target = "userID", source="userId"),
    @Mapping(target = "categoriesHasUsers", source = "categories"),
    @Mapping(target = "country", ignore = true),
    @Mapping(target="city", source="cityDTO"),
  })
  public abstract User toUser(UserDTO userDTO) throws EntityNotFoundException;
  public abstract List<User> toUsers(List<UserDTO> users);


  public User updateUserFromUserDto(UserDTO userDTO, User user) throws EntityNotFoundException{
    userDTO.setUserId(user.getUserID());
    User newUser = toUser(userDTO);
    user.setUsername(newUser.getUsername());

    user.setCity(cityMapper.toCityByCityDTOId( userDTO.getCityDTO() ) );

//        user.setMedias(newUser.getMedias());
//        user.setCategoriesHasUsers( newUser.getCategoriesHasUsers() );
    user.setFirstName( newUser.getFirstName() );
    user.setLastName( newUser.getLastName() );
    user.setInformationAboutYourself( newUser.getInformationAboutYourself() );
    user.setEmail( newUser.getEmail() );
    return user;
  }

  @Mappings({
    @Mapping(target = "userID", ignore = true),
    @Mapping(target = "country", ignore = true),
    @Mapping(target = "medias", ignore = true),
    @Mapping(target = "categoriesHasUsers", source = "categories"),
    @Mapping(target = "password", ignore = true),
    @Mapping(target="city", source="cityDTO"),
  })
  public abstract User updateUserFromUserDtoMapper(UserDTO userDTO, @MappingTarget User user) throws EntityNotFoundException;


  private User findUserByUserDTO(UserDTO userDTO){
    return userRepository.findById(userDTO.getUserId()).get();
  }

  public List<User> toUsersByIdUserDTO(List<UserDTO> users){
    if ( users == null ) {
      return null;
    }
    List<User> list = new ArrayList<>( users.size() );
    for ( UserDTO user : users ) {
      list.add( findUserByUserDTO( user ) );
    }
    return list;
  }
}
