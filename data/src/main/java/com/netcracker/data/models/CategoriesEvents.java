package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="categories")
public class CategoriesEvents {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categories_id", unique = true, nullable = false)
    private Integer categoryEventID;

    @Column(name="name_category", nullable = false)
    private String nameCategory;

    @Column(name="color")
    private String color;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private List<EventsHasCategories> eventsHasCategories = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoriesEvents", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private List<CategoriesHasUsers> categoriesHasUsers = new ArrayList<>();

    public void setEventsHasCategories(List<EventsHasCategories> eventsHasCategories) {
        this.eventsHasCategories = eventsHasCategories;
        for (EventsHasCategories eventsHasCategory : eventsHasCategories) {
            eventsHasCategory.setCategory(this);
        }
    }

    public void setCategoriesHasUsers(List<CategoriesHasUsers> categoriesHasUsers) {
        this.categoriesHasUsers = categoriesHasUsers;
        for (CategoriesHasUsers categoriesHasUser : categoriesHasUsers) {
            categoriesHasUser.setCategoriesEvents(this);
        }
    }

    public void addCategoryHasUsers(CategoriesHasUsers categoriesHasUsers){
        categoriesHasUsers.setCategoriesEvents(this);
        this.categoriesHasUsers.add(categoriesHasUsers);

////      //проверка на дубликат
//        HashSet<Object> seen = new HashSet<>();
//        this.categoriesHasUsers.removeIf(e -> !seen.add(e.getCategoriesEvents()));
    }

    public void addEventsHasCategories(EventsHasCategories eventsHasCategories){
        eventsHasCategories.setCategory(this);
        this.eventsHasCategories.add(eventsHasCategories);
    }
}
