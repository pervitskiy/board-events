package com.netcracker.data.models;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(CategoriesEvents.class)
public class CategoriesEvents_ {
	public static volatile SingularAttribute<CategoriesEvents, Integer> categoryEventID;
	public static volatile SingularAttribute<CategoriesEvents, String> name;
}
