package com.netcracker.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="_cities")
public class City implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "city_uuid_id", unique = true, nullable = false)
    private UUID cityUuidId;

    @SequenceGenerator(name = "cityIdSeq", sequenceName = "city_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cityIdSeq")
    @Column(name = "city_id", unique = true, nullable = false)
    private Integer cityId;


    @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "country_id" , referencedColumnName = "country_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Country country;

    @Column(name="important")
    private boolean important;

    @Column(name="title_ru")
    private String titleRu;
    @Column(name="area_ru")
    private String areaRu;
    @Column(name="region_ru")
    private String regionRu;

    @Column(name="title_ua")
    private String titleUa;
    @Column(name="area_ua")
    private String areaUa;
    @Column(name="region_ua")
    private String regionUa;

    @Column(name="title_be")
    private String titleBe;
    @Column(name="area_be")
    private String areaBe;
    @Column(name="region_be")
    private String regionBe;

    @Column(name="title_en")
    private String titleEn;
    @Column(name="area_en")
    private String areaEn;
    @Column(name="region_en")
    private String regionEn;

    @Column(name="title_es")
    private String titleEs;
    @Column(name="area_es")
    private String areaEs;
    @Column(name="region_es")
    private String regionEs;

    @Column(name="title_pt")
    private String titlePt;
    @Column(name="area_pt")
    private String areaPt;
    @Column(name="region_pt")
    private String regionPt;

    @Column(name="title_de")
    private String titleDe;
    @Column(name="area_de")
    private String areaDe;
    @Column(name="region_de")
    private String regionDe;

    @Column(name="title_fr")
    private String titleFr;
    @Column(name="area_fr")
    private String areaFr;
    @Column(name="region_fr")
    private String regionFr;

    @Column(name="title_it")
    private String titleIt;
    @Column(name="area_it")
    private String areaIt;
    @Column(name="region_it")
    private String regionIt;

    @Column(name="title_pl")
    private String titlePl;
    @Column(name="area_pl")
    private String areaPl;
    @Column(name="region_pl")
    private String regionPl;

    @Column(name="title_ja")
    private String titleJa;
    @Column(name="area_ja")
    private String areaJa;
    @Column(name="region_ja")
    private String regionJa;

    @Column(name="title_lt")
    private String titleLt;
    @Column(name="area_lt")
    private String areaLt;
    @Column(name="region_lt")
    private String regionLt;

    @Column(name="title_lv")
    private String titleLv;
    @Column(name="area_lv")
    private String areaLv;
    @Column(name="region_lv")
    private String regionLv;

    @Column(name="title_cz")
    private String titleCz;
    @Column(name="area_cz")
    private String areaCz;
    @Column(name="region_cz")
    private String regionCz;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "region_id", referencedColumnName= "region_id", nullable = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Region region;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private List<Event> events = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "city", orphanRemoval = true)
    private List<User> users = new ArrayList<>();

    public void setUsers(List<User> users) {
      if (users!=null){
        this.users.addAll(users);
        for (User user : users) {
          user.setCity(this);
        }
      }
    }

    public void addUser(User user){
      if (user!=null){
        this.users.add(user);
      }
    }


    public void setEvents(List<Event> events) {
          this.events.addAll(events);
          for (Event event : events) {
              event.setCity(this);
          }
      }

    public void addEvent(Event event){
          this.events.add(event);
      }
}
