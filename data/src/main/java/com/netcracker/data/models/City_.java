package com.netcracker.data.models;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(City.class)
public class City_ {
	public static volatile SingularAttribute<City,Integer> cityId;
	public static volatile SingularAttribute<City,Country> country;
}
