package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="comments")
public class Comment {

    @Id
    @GeneratedValue
    @Column(name = "comments_id", unique = true, nullable = false)
    private UUID commentId;

    @Column(name="message", nullable = false)
    private String message;

    @CreationTimestamp
    @Column(name="comments_date", nullable = false)
    private LocalDateTime commentDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "events_event_id", nullable = false)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "users_user_id", nullable = false)
    private User user;
}
