package com.netcracker.data.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Data
@Entity
@DynamicInsert
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="_countries")
public class Country implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "country_uuid_id", unique = true, nullable = false)
    private UUID countryUuidId;

    @SequenceGenerator(name = "countryIdSeq", sequenceName = "country_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "countryIdSeq")
    @Column(name = "country_id", unique = true, nullable = false)
    private Integer countryId;

    @Column(name="title_ru", nullable = false)
    private String titleRu;
    @Column(name="title_ua")
    private String titleUa;
    @Column(name="title_be")
    private String titleBe;
    @Column(name="title_en")
    private String titleEn;
    @Column(name="title_es")
    private String titleEs;
    @Column(name="title_pt")
    private String titlePt;
    @Column(name="title_de")
    private String titleDe;
    @Column(name="title_fr")
    private String titleFr;
    @Column(name="title_it")
    private String titleIt;
    @Column(name="title_pl")
    private String titlePl;
    @Column(name="title_ja")
    private String titleJa;
    @Column(name="title_lt")
    private String titleLt;
    @Column(name="title_lv")
    private String titleLv;
    @Column(name="title_cz")
    private String titleCz;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private List<City> cities = new ArrayList<>();

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "country", orphanRemoval = true)
    private List<Region> regions = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "country", orphanRemoval = true)
    private List<User> users = new ArrayList<>();

    public void setUsers(List<User> users) {
      if (users != null) {
        this.users = users;
        for (User user : users) {
          user.setCountry(this);
        }
      }
    }

    public void addUser(User user){
      if (user!=null){
        this.users.add(user);
      }
    }

    public void addRegions(Region region){
          region.setCountry(this);
          this.regions.add(region);
      }

      public void addCities(City city){
          city.setCountry(this);
          this.cities.add(city);
      }



      public void setRegions(List<Region> regions) {
          this.regions = regions;
          for (Region region : regions) {
              region.setCountry(this);
          }
      }

      public void setCities(List<City> cities) {
          this.cities = cities;
          for (City city : cities) {
              city.setCountry(this);
          }
      }
}
