package com.netcracker.data.models;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.UUID;

@StaticMetamodel(Country.class)
public class Country_ {
	public static volatile SingularAttribute<Country,Integer> countryId;
	public static volatile SingularAttribute<Country,UUID> countryUuidId;
	public static volatile SingularAttribute<Country,String> titleRu;
}
