package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.*;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@Table(name = "events")
public class Event {

	@Id
	@GeneratedValue
	@Column(name = "event_id", unique = true, nullable = false)
	private UUID eventId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_time", nullable = false)
	private Date dateTime;

	@Column(name = "place", nullable = false)
	private String place;

	@Column(name = "brief_description", nullable = false)
	private String briefDescription;

	@Column(name = "full_description", nullable = false)
	private String fullDescription;

	@Column(name = "external_event", nullable = true)
	private String externalEvent;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", nullable = true)
	private Date creation_date;

	@Column(name = "vk_id", unique = true)
	private Integer vkEventId;

	@Column(name = "culture_id", unique = true)
	private Integer cultureEventId;

	@Column(name = "members_count", columnDefinition = "integer default 0")
	private Integer membersCount;

	public void setMembersCount(Integer membersCount) {
		if (membersCount != null)
			this.membersCount = membersCount;
	}

	@Column(name = "rating", columnDefinition = "double precision default 0")
	private Double rating;

	public void setRating(Double rating) {
		if (rating != null)
			this.rating = rating;
	}

	@Column(name = "age_restriction")
	private String ageRestriction;

	@Column(name = "imageurl")
	private String securityUrl;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public void setCity(City city) {
		if (city != null) {
			this.city = city;
			//можно добавить провекру, чтоб не было повторяющихся событий в городе
			city.addEvent(this);
		}
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "cities_city_id", referencedColumnName = "city_id", nullable = false)
	private City city;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<Media> media = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "eventCategory", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<EventsHasCategories> eventsCategories = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<Comment> comments = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
	private List<PaymentInformation> paymentInformations = new ArrayList<>();

	//	@Fetch(FetchMode.JOIN)
//	@Fetch(FetchMode.SELECT)
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@OrderColumn
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<UserHasEvents> users = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<LikeUserOnEvent> likeUserOnEvents = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<FavoritesUserOnEvent> favoritesUserOnEvents = new ArrayList<>();

	public void setLikeUserOnEvents(List<LikeUserOnEvent> likeUserOnEvents) {
		if (likeUserOnEvents != null) {
			if (!this.likeUserOnEvents.isEmpty()) {
				this.likeUserOnEvents.clear();
			}
			this.likeUserOnEvents.addAll(likeUserOnEvents);
			for (LikeUserOnEvent likeUserOnEvent : likeUserOnEvents) {
				likeUserOnEvent.setEvent(this);
			}
		}
	}

	public void setFavoritesUserOnEvents(List<FavoritesUserOnEvent> favoritesUserOnEvents) {
		if (favoritesUserOnEvents != null) {
			if (!this.favoritesUserOnEvents.isEmpty()) {
				this.favoritesUserOnEvents.clear();
			}
			this.favoritesUserOnEvents.addAll(favoritesUserOnEvents);
			for (FavoritesUserOnEvent favoritesUserOnEvent : favoritesUserOnEvents) {
				favoritesUserOnEvent.setEvent(this);
			}
		}
	}

	public void deleteFromMembersCount() {
		this.membersCount = membersCount - 1;
	}

	public void addToMembersCount() {
		this.membersCount = membersCount + 1;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "eventOrganizers", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<OrganizersHasEvents> organizers = new ArrayList<>();

	//	@Fetch(FetchMode.JOIN)
//	@Fetch(FetchMode.SELECT)
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@OrderColumn
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
	private List<Rating> ratings = new ArrayList<>();

	public void setRatings(List<Rating> ratings) {
		if (ratings != null) {
			if (!this.ratings.isEmpty()) {
				this.ratings.clear();
			}
			this.ratings.addAll(ratings);
			for (Rating rating : ratings) {
				rating.setEvent(this);
			}
		}
	}

	public void setComments(List<Comment> comments) {
		if (comments != null) {
			if (!this.comments.isEmpty()) {
				this.comments.clear();
			}
			this.comments.addAll(comments);
			for (Comment comment : comments) {
				comment.setEvent(this);
			}
		}
	}

	public void addComment(Comment comment) {
		comment.setEvent(this);
		this.comments.add(comment);
	}

	public void setMedia(List<Media> media) {
		if (media != null) {
			if (!this.media.isEmpty()) {
				this.media.clear();
			}
			this.media.addAll(media);
			for (Media media1 : media) {
				media1.setEvent(this);
			}
		}
	}

	public void setUsers(List<UserHasEvents> users) {
		if (users != null) {
			if (!this.users.isEmpty()) {
				this.users.clear();
			}
			this.users.addAll(users);
			for (UserHasEvents user : users) {
				user.setEvent(this);
			}
		}
	}

	public void addComments(List<Comment> comments) {
		if (comments != null && !comments.isEmpty()) {
			this.comments.addAll(comments);
		}
		for (Comment comment : comments) {
			comment.setEvent(this);
		}
	}

	public void addLikeUserOnEvents(List<LikeUserOnEvent> likeUserOnEvents) {
		if (likeUserOnEvents != null && !likeUserOnEvents.isEmpty()) {
			this.likeUserOnEvents.addAll(likeUserOnEvents);
		}
		for (LikeUserOnEvent likeUserOnEvent : likeUserOnEvents) {
			likeUserOnEvent.setEvent(this);
		}
	}

	public void addFavoritesUserOnEvents(List<FavoritesUserOnEvent> favoritesUserOnEvents) {
		if (favoritesUserOnEvents != null && !favoritesUserOnEvents.isEmpty()) {
			this.favoritesUserOnEvents.addAll(favoritesUserOnEvents);
		}
		for (FavoritesUserOnEvent favoritesUserOnEvent : favoritesUserOnEvents) {
			favoritesUserOnEvent.setEvent(this);
		}
	}

	public void addRatings(List<Rating> ratings) {
		if (ratings != null && !ratings.isEmpty()) {
			this.ratings.addAll(ratings);
		}
		for (Rating rating : ratings) {
			rating.setEvent(this);
		}
	}

	public void addEventsCategories(List<EventsHasCategories> eventsCategories) {
		if (eventsCategories != null && !eventsCategories.isEmpty()) {
			this.eventsCategories.addAll(eventsCategories);
		}
		for (EventsHasCategories eventsCategory : eventsCategories) {
			eventsCategory.setEventCategory(this);
		}
	}

	public void addUsers(List<UserHasEvents> users) {
		if (users != null && !users.isEmpty()) {
			this.users.addAll(users);
		}
		for (UserHasEvents user : users) {
			user.setEvent(this);
		}
	}

	public void addOrganizers(List<OrganizersHasEvents> organizers) {
		if (organizers != null && !organizers.isEmpty()) {
			this.organizers.addAll(organizers);
		}
		for (OrganizersHasEvents organizer : organizers) {
			organizer.setEventOrganizers(this);
		}
	}

	public void addMedias(List<Media> medias) {
		if (medias != null && !medias.isEmpty()) {
			this.media.addAll(medias);
		}
		for (Media media1 : medias) {
			media1.setEvent(this);
		}
	}

	public void setOrganizers(List<OrganizersHasEvents> organizers) {
		if (organizers != null) {
			if (!this.organizers.isEmpty()) {
				this.organizers.clear();
			}
			this.organizers.addAll(organizers);
			for (OrganizersHasEvents organizersHasEvents : organizers) {
				organizersHasEvents.setEventOrganizers(this);
			}
		}
	}

	public void setEventsCategories(List<EventsHasCategories> eventsCategories) {
		if (eventsCategories != null) {
			if (!this.eventsCategories.isEmpty()) {
				this.eventsCategories.clear();
			}
			HashSet<Object> seen = new HashSet<>();
			eventsCategories.removeIf(e -> !seen.add(e.getCategory()));
			this.eventsCategories.addAll(eventsCategories);
			for (EventsHasCategories eventsCategory : eventsCategories) {
				eventsCategory.setEventCategory(this);
			}
		}
	}
}
