package com.netcracker.data.models;

import com.netcracker.data.models.Event;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;
import java.util.UUID;

@StaticMetamodel(Event.class)
public class Event_ {
	public static volatile SingularAttribute<Event, City> city;
	public static volatile SingularAttribute<Event, Integer> membersCount;
	public static volatile SingularAttribute<Event, Double> rating;
	public static volatile SingularAttribute<Event, Rating> ratings;
	public static volatile SingularAttribute<Event, String> briefDescription;
	public static volatile SingularAttribute<Event, String> fullDescription;
	public static volatile SingularAttribute<Event, Date> dateTime;
	public static volatile ListAttribute<Event, EventsHasCategories> eventsCategories;

}
