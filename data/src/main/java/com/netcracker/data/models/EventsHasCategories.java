package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="events_has_categories")
public class EventsHasCategories {

    @Id
    @GeneratedValue
    @Column(name = "events_has_categories_id", unique = true, nullable = false)
    private UUID eventsHasCategoriesID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "events_event_id", nullable = false)
    private Event eventCategory;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name = "categories_categories_id", nullable = false)
    private CategoriesEvents category;

}
