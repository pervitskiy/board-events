package com.netcracker.data.models;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(EventsHasCategories.class)
public class EventsHasCategories_ {
	public static volatile SingularAttribute<EventsHasCategories, CategoriesEvents> category;
}
