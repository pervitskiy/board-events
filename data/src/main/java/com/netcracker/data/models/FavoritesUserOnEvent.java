package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="favorites_user_on_event")
public class FavoritesUserOnEvent {

  @Id
  @GeneratedValue
  @Column(name = "favorites_user_on_event_id", unique = true, nullable = false)
  private UUID likeUserId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "events_event_id", nullable = false)
  private Event event;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "users_user_id", nullable = false)
  private User user;
}
