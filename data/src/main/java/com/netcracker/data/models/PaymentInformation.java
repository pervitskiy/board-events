package com.netcracker.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="payment_information")
public class PaymentInformation {

    @Id
    @GeneratedValue
    @Column(name = "payment_information_id", unique = true, nullable = false)
    private UUID paymentInformationID;

    @Column(name="datetime", nullable = false)
    private LocalDateTime createDate;

    @Column(name="status", nullable = false)
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "events_event_id", nullable = false)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "users_user_id", nullable = false)
    private User user;

    @Column(name = "email_address", nullable = false)
    private String emailAddress;

    @Column(name = "value")
    private String value;
}
