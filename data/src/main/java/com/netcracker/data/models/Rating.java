package com.netcracker.data.models;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "rating")
public class Rating {

	@Id
	@GeneratedValue
	@Column(name = "id_rating", unique = true, nullable = false)
	private UUID ratingId;

	@Column(name = "val_rating", nullable = false)
	private float valRating;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "events_event_id")
	private Event event;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "users_user_id")
	private User user;
}
