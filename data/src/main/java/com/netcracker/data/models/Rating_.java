package com.netcracker.data.models;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Rating.class)
public class Rating_ {
	public static volatile SingularAttribute<Rating,Float> valRating;
}
