package com.netcracker.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="_regions")
public class Region implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "region_uuid_id", unique = true, nullable = false)
	private UUID regionUuidId;

	@Column(name="title_ru", nullable = false)
	private String titleRu;
	@Column(name="title_ua", nullable = false)
	private String titleUa;
	@Column(name="title_be", nullable = false)
	private String titleBe;
	@Column(name="title_en", nullable = false)
	private String titleEn;
	@Column(name="title_es", nullable = false)
	private String titleEs;
	@Column(name="title_pt", nullable = false)
	private String titlePt;
	@Column(name="title_de", nullable = false)
	private String titleDe;
	@Column(name="title_fr", nullable = false)
	private String titleFr;
	@Column(name="title_it", nullable = false)
	private String titleIt;
	@Column(name="title_pl", nullable = false)
	private String titlePl;
	@Column(name="title_ja", nullable = false)
	private String titleJa;
	@Column(name="title_lt", nullable = false)
	private String titleLt;
	@Column(name="title_lv", nullable = false)
	private String titleLv;
	@Column(name="title_cz", nullable = false)
	private String titleCz;

	@SequenceGenerator(name = "regionIdSeq", sequenceName = "region_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "regionIdSeq")
	@Column(name = "region_id", unique = true, nullable = false)
	private Integer regionId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "country_id", referencedColumnName = "country_id")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Country country;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "region")
	private List<City> cities;

	public void setCities(List<City> cities) {
		this.cities = cities;
		for (City city : cities) {
			city.setRegion(this);
		}
	}

	public void addCities(City city){
		this.cities.add(city);
	}
}
