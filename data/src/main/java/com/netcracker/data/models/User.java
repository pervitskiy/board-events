package com.netcracker.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.annotations.OrderBy;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="users")
public class User implements Serializable {

  @Id
  @GeneratedValue
  @Column(name = "user_id", unique = true, nullable = false)
  private UUID userID;

  @Column(name="first_name", nullable = false)
  private String firstName;

  @Column(name="last_name", nullable = false)
  private String lastName;

  @Column(name="information_about_yourself", nullable = true)
  private String informationAboutYourself;

  @Column(name="email", nullable = false)
  private String email;

  @Column(name = "prem", nullable = false)
  private Boolean prem;
  
  @Column(name="password", nullable = false)
  private String password;

  @Column(name = "username", unique = true, nullable = false)
  private String username;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<UserHasEvents> userHasEvents = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<LikeUserOnEvent> likeUserOnEvents = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<FavoritesUserOnEvent> favoritesUserOnEvents = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "organizers", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<OrganizersHasEvents> organizers = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<Rating> ratings = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  @OrderBy(clause = "dateAdded DESC NULLS LAST")
  private List<Media> medias = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  @OrderBy(clause = "commentDate NULLS LAST")
  private List<Comment> comments = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<PaymentInformation> paymentInformations = new ArrayList<>();

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
  private List<CategoriesHasUsers> categoriesHasUsers = new ArrayList<>();;

  @ManyToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "country_id" , referencedColumnName = "country_id")
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private Country country;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(name = "cities_city_id", referencedColumnName = "city_id")
  private City city;

  @Temporal(TemporalType.TIMESTAMP)
  @JoinColumn(name = "last_date_prem")
  private Date lastDatePrem;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  public void setCity(City city) {
    if (city!=null) {
      city.addUser(this);
      this.city = city;
    }
  }

  public void setCountry(Country country) {
      if (country!=null)
        country.addUser(this);
        this.country = country;
    }

  public void setLikeUserOnEvents(List<LikeUserOnEvent> likeUserOnEvents) {
    if (likeUserOnEvents != null) {
      if (!this.likeUserOnEvents.isEmpty()) {
        this.likeUserOnEvents.clear();
      }
      this.likeUserOnEvents.addAll(likeUserOnEvents);
      for (LikeUserOnEvent likeUserOnEvent : likeUserOnEvents) {
        likeUserOnEvent.setUser(this);
      }
    }
  }

  public void setFavoritesUserOnEvents(List<FavoritesUserOnEvent> favoritesUserOnEvents) {
    if (favoritesUserOnEvents != null) {
      if (!this.favoritesUserOnEvents.isEmpty()) {
        this.favoritesUserOnEvents.clear();
      }
      this.favoritesUserOnEvents.addAll(favoritesUserOnEvents);
      for (FavoritesUserOnEvent favoritesUserOnEvent : favoritesUserOnEvents) {
        favoritesUserOnEvent.setUser(this);
      }
    }
  }

  public void setUserHasEvents(List<UserHasEvents> userHasEvents) {
    if (userHasEvents != null) {
      if (!this.userHasEvents.isEmpty()) {
        this.userHasEvents.clear();
      }
      this.userHasEvents.addAll(userHasEvents);
      for (UserHasEvents userHasEvent : userHasEvents) {
        userHasEvent.setUser(this);
      }
    }
  }

  public void setOrganizers(List<OrganizersHasEvents> organizers) {
    if (organizers != null) {
      if (!this.organizers.isEmpty()) {
        this.organizers.clear();
      }
      this.organizers.addAll(organizers);
      for (OrganizersHasEvents organizer : organizers) {
        organizer.setOrganizers(this);
      }
    }
  }

  public void setRatings(List<Rating> ratings) {
    if (ratings!=null){
      if (!this.ratings.isEmpty()){
        this.ratings.clear();
      }
      this.ratings.addAll(ratings);
      for (Rating rating : ratings) {
        rating.setUser(this);
      }
    }
  }

  public void setComments(List<Comment> comments) {
    if (comments!=null) {
      if (!comments.isEmpty()){
        this.comments.clear();
      }
      this.comments.addAll(comments);
      for (Comment comment : comments) {
        comment.setUser(this);
      }
    }
  }

  public void addComments(Comment comment){
    comment.setUser(this);
    this.comments.add(comment);
  }

  public void addPaymentInformation(PaymentInformation paymentInformation){
    paymentInformation.setUser(this);
    this.paymentInformations.add(paymentInformation);
  }

  public void setPaymentInformations(List<PaymentInformation> paymentInformations) {
    this.paymentInformations = paymentInformations;
    for (PaymentInformation paymentInformation : paymentInformations) {
      paymentInformation.setUser(this);
    }
  }

  public void setCategoriesHasUsers(List<CategoriesHasUsers> categoriesHasUsers) {
    if (categoriesHasUsers != null) {
      if (!this.categoriesHasUsers.isEmpty()) {
        this.categoriesHasUsers.clear();
      }
      HashSet<Object> seen = new HashSet<>();
      categoriesHasUsers.removeIf(e -> !seen.add(e.getCategoriesEvents()));
      this.categoriesHasUsers.addAll(categoriesHasUsers);
      for (CategoriesHasUsers categoriesHasUser : categoriesHasUsers) {
        categoriesHasUser.setUser(this);
      }
    }
  }

  public void addUserHasEvents(UserHasEvents userHasEvents){
    userHasEvents.setUser(this);
    this.userHasEvents.add(userHasEvents);
  }

  public void addLikeUserOnEvents(LikeUserOnEvent likeUserOnEvent){
    likeUserOnEvent.setUser(this);
    this.likeUserOnEvents.add(likeUserOnEvent);
  }

  public void addFavoritesUserOnEvent(FavoritesUserOnEvent favoritesUserOnEvents){
    favoritesUserOnEvents.setUser(this);
    this.favoritesUserOnEvents.add(favoritesUserOnEvents);
  }

  public void addOrganizersHasEvents(OrganizersHasEvents organizersHasEvents){
    organizersHasEvents.setOrganizers(this);
    this.organizers.add(organizersHasEvents);
  }

  public void addCategoriesHasUsers(CategoriesHasUsers categoriesHasUsers) {
    if (categoriesHasUsers != null){
      categoriesHasUsers.setUser(this);
      this.categoriesHasUsers.add(categoriesHasUsers);
      HashSet<Object> seen = new HashSet<>();
      this.categoriesHasUsers.removeIf(e -> !seen.add(e.getCategoriesEvents()));
      System.out.println(this.categoriesHasUsers.size());
    }
  }

  public void setMedias(List<Media> medias) {
    if (medias != null) {
      if (!this.medias.isEmpty()) {
        this.medias.clear();
      }
      this.medias.addAll(medias);
      for (Media m : medias) {
        m.setUser(this);
      }
    }
  }

  public void addMediaUser(Media media) {
    this.medias.add(media);
    media.setUser(this);
  }
}
