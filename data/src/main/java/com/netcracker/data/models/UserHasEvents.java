package com.netcracker.data.models;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name="users_has_events")
public class UserHasEvents {
    @Id
    @GeneratedValue
    @Column(name = "users_has_event_id", unique = true, nullable = false)
    private UUID userHasEventsId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "events_event_id", nullable = false)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "users_user_id", nullable = false)
    private User user;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_added")
    private Date dateAdded;
}
