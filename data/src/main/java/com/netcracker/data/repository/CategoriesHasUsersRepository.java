package com.netcracker.data.repository;

import com.netcracker.data.models.CategoriesEvents;
import com.netcracker.data.models.CategoriesHasUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CategoriesHasUsersRepository extends JpaRepository<CategoriesHasUsers, UUID> {
}
