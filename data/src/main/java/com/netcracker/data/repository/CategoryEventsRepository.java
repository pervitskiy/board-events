package com.netcracker.data.repository;


import com.netcracker.data.models.CategoriesEvents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryEventsRepository extends JpaRepository<CategoriesEvents, Integer> {
  Optional<CategoriesEvents> findByNameCategory(String name);
}
