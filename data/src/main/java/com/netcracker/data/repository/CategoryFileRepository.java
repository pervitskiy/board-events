package com.netcracker.data.repository;

import com.netcracker.data.models.CategoryFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryFileRepository extends JpaRepository<CategoryFile, Integer> {
}
