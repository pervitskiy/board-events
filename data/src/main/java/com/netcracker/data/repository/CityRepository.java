package com.netcracker.data.repository;


import com.netcracker.data.models.City;
import com.netcracker.data.models.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CityRepository extends JpaRepository<City, UUID> {
    @Query(value = "select c.* from _cities c where c.country_id=:countyId and substring(c.title_ru, 1, :lengthChar)=:stringNameCity order by c.title_ru limit 30 ",
            nativeQuery = true)
    List<City> getListCityByCountryId (@Param("countyId") Integer countyId, @Param("lengthChar") Integer lengthChar, @Param("stringNameCity")String stringNameCity );

    @Query(value = "select c.* from _cities c where c.country_id=:countyId and c.important is true order by c.title_ru",
    nativeQuery = true)
    List<City> getListDefaultCityByCountryId(@Param("countyId") Integer countryId);
}
