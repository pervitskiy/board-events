package com.netcracker.data.repository;


import com.netcracker.data.models.Comment;
import com.netcracker.data.models.FavoritesUserOnEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CommentsRepository extends JpaRepository<Comment, UUID> {

  @Query(value = "select c.* from comments c where c.users_user_id=:userId and c.events_event_id=:eventId",
    nativeQuery = true)
  Comment findByCommentFromEvent(@Param("userId") UUID userId, @Param("eventId") UUID eventId);

  Comment findByCommentId(UUID commendId);
}
