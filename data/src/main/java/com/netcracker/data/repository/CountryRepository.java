package com.netcracker.data.repository;

import com.netcracker.data.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CountryRepository extends JpaRepository<Country, UUID> {
  Country findByTitleRu(String titleRu);
  List<Country> findAllByOrderByTitleRu ();
}
