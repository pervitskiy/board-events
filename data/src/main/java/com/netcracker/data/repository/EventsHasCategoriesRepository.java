package com.netcracker.data.repository;


import com.netcracker.data.models.EventsHasCategories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EventsHasCategoriesRepository extends JpaRepository<EventsHasCategories, UUID> {
}
