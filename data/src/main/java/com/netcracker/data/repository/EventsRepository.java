package com.netcracker.data.repository;

import com.netcracker.data.dto.UserEventsDTO;
import com.netcracker.data.models.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.print.attribute.standard.PagesPerMinute;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface EventsRepository extends JpaRepository<Event, UUID>, SearchRepositoryCustom, JpaSpecificationExecutor<Event> {

//	@Query("select e from Event e WHERE current_timestamp <e.dateTime ORDER BY e.dateTime")
//	Page<Event> findAll(Pageable pageable);

	//  @Query("select e from Event e JOIN UserHasEvents u ON WHERE e.eventId = (SELECT u.events_event_id FROM UserHasEvents u WHERE u.users_user_id ) ORDER BY e.dateTime")
	@Query("select e from Event e JOIN UserHasEvents u ON u.event = e.eventId and u.user = ?1 ORDER BY e.dateTime")
	List<UserEventsDTO> findByUserId(UUID uuid);

  @Query("select e from Event e where e.dateTime > current_date ORDER BY e.dateTime")
	List<Event> findAllEvents();

	@Query(
					value =
									"SELECT e.*, ts_rank(to_tsvector(\"brief_description\"), plainto_tsquery(:query)) " +
													"FROM events e " +
													"WHERE to_tsvector(\"brief_description\") @@ plainto_tsquery(:query)",
//													+ "AND citites_city_id = :city; ",
					nativeQuery = true
	)
	Page<Event> querySearch(@Param("query") String query, Pageable pageable);
//	, @Param("city") UUID city);

	@Query(
					value =
									"SELECT e.*, ts_rank(to_tsvector(\"brief_description\"), plainto_tsquery(:query)) as rank " +
													"FROM events e " +
													"WHERE to_tsvector(\"brief_description\") @@ plainto_tsquery(:query) " +
													"AND e.date_time > :time ",
//													+ "AND citites_city_id = :city; ",
					nativeQuery = true
	)
	Page<Event> findByDateTimeAfter(@Param("time") Date time, @Param("query") String query, Pageable pageable);
//	, @Param("city") UUID city);

	Page<Event> findByDateTimeAfter(Date time, Pageable pageable);

	Page<Event> findAll(Pageable pageable);

	@EntityGraph(
					type = EntityGraph.EntityGraphType.FETCH,
					attributePaths = {
									"eventId",
									"dateTime",
									"briefDescription",
									"fullDescription",
									"eventsCategories","eventsCategories.category", "eventsCategories.category.categoryEventID","eventsCategories.eventCategory","eventsCategories.eventsHasCategoriesID",
									"rating",
									"city","city.cityId"
					}
	)
	Page<Event> findAll(@Nullable Specification<Event> spec, Pageable pageable);
}
