package com.netcracker.data.repository;

import com.netcracker.data.models.FavoritesUserOnEvent;
import com.netcracker.data.models.LikeUserOnEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FavoritesUserOnEventRepository extends JpaRepository<FavoritesUserOnEvent, UUID> {
	@Query(value = "select f.* from favorites_user_on_event f where f.users_user_id=:userId and f.events_event_id=:eventId",
					nativeQuery = true)
	FavoritesUserOnEvent findByFavoriteFromEvent(@Param("userId") UUID userId, @Param("eventId") UUID eventId);

	@Query(value = "select u.*, e.rating,e.members_count,e.date_time from favorites_user_on_event u join events e on e.event_id = u.events_event_id where u.users_user_id=:userId ",
					nativeQuery = true)
	Page<FavoritesUserOnEvent> getListFavouritesUserOnEventPageable(@Param("userId") UUID userId, Pageable pageable);
}
