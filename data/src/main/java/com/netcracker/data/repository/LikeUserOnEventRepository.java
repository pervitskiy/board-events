package com.netcracker.data.repository;

import com.netcracker.data.models.Event;
import com.netcracker.data.models.LikeUserOnEvent;
import com.netcracker.data.models.UserHasEvents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LikeUserOnEventRepository extends JpaRepository<LikeUserOnEvent, UUID> {

  @Query(value = "select l.* from like_user_on_event l where l.users_user_id=:userId and l.events_event_id=:eventId",
    nativeQuery = true)
  LikeUserOnEvent findByLikeFromEvent(@Param("userId") UUID userId, @Param("eventId") UUID eventId);
}
