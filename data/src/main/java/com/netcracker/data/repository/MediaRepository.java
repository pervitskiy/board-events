package com.netcracker.data.repository;

import com.netcracker.data.models.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MediaRepository extends JpaRepository<Media, UUID> {

    @Query(value = "select m.* from media m where m.users_user_id=:userId and m.events_event_id is null",
           nativeQuery = true)
    List<Media> getListMediaByUserId(@Param("userId") UUID userId);

}
