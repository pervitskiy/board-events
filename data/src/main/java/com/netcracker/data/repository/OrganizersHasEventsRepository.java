package com.netcracker.data.repository;

import com.netcracker.data.models.OrganizersHasEvents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface OrganizersHasEventsRepository extends JpaRepository<OrganizersHasEvents, UUID> {
  @Query(value = "select count(*) as cnt from organizers_has_events o where o.users_user_id = :userId and o.date_added > current_date-30 group by o.users_user_id;", nativeQuery = true)
  Integer findCountEventsByUserUuid(@Param("userId") UUID userId);
}
