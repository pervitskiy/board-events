package com.netcracker.data.repository;

import com.netcracker.data.dto.RatingQueryDTO;
import com.netcracker.data.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository

public interface RatingRepository extends JpaRepository<Rating, UUID>, RatingRepositoryCustom {

//	@Query(value = "select count(*), from rating r where events_event_id = :events_event_id;",nativeQuery = true)
//	List<Rating> findRatingsByEventId(@Param("events_event_id") UUID eventId);

}
