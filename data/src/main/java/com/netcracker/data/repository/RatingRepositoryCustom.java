package com.netcracker.data.repository;

import com.netcracker.data.dto.RatingQueryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface RatingRepositoryCustom {
	@Query(value = "select avg(val_rating) as avg from rating r where events_event_id = :eventId ;", nativeQuery = true)
	Double findRatingsByEventUuid(@Param("eventId") UUID eventId);
}
