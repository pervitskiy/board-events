package com.netcracker.data.repository;

import com.netcracker.data.models.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository

public interface RegionRepository extends JpaRepository<Region, UUID> {
}
