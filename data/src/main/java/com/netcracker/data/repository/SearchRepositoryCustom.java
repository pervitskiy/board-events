package com.netcracker.data.repository;

import com.netcracker.data.models.Event;

import java.util.List;
import java.util.Map;

public interface SearchRepositoryCustom {
	List<Event> findEventsByParams(Map<String, Object> params);
}
