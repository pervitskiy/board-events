package com.netcracker.data.repository;

import com.netcracker.data.models.Event;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SearchRepositoryCustomImpl implements SearchRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	Predicate finalPredicate;
	Predicate cityPred;
	boolean city = false;
	Predicate descriptionPred;

	@Override
	public List<Event> findEventsByParams(Map<String, Object> params) {

//		SessionFactory factory = HibernateUtil.getHibernateSession().getSessionFactory();
		SessionFactory factory = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
		final Session session = factory.openSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Event> query = cb.createQuery(Event.class);
		Root<Event> event = query.from(Event.class);

		//add predicates to query
		ArrayList<Predicate> predicates = new ArrayList<>();

		List<Predicate> allPredicates = new ArrayList<>();
		ArrayList<Predicate> briefDesc = new ArrayList<>();
		ArrayList<Predicate> fullDesc = new ArrayList<>();

		if (params.get("city") != null) {
			cityPred = cb.equal(event.get("cities_city_id"), String.valueOf(params.get("city")));
			city = true;
		}

		String sqlQuery = String.valueOf(params.get("query"));
		List<String> queryList = new ArrayList<>();
		if (sqlQuery.contains(" ")) {
			System.out.println("contains space");
			queryList = Arrays.asList(sqlQuery.split("\\s+"));
		}

		if (!queryList.isEmpty()) {
			int i = 0;
			ArrayList<String> modified = new ArrayList<>();
			for (String str : queryList) {
				modified.add("%" + str.toLowerCase(Locale.ROOT) + "%");
			}
			queryList = modified;
			System.out.println(queryList);

			for (String str : queryList) {
				briefDesc.add(cb.like(cb.lower(event.get("briefDescription")), str));
				fullDesc.add(cb.like(cb.lower(event.get("fullDescription")), str));
			}
			allPredicates = Stream.of(briefDesc, fullDesc)
							.flatMap(Collection::stream)
							.collect(Collectors.toList());

			descriptionPred = cb.or(
							cb.and(briefDesc.toArray(Predicate[]::new)),
							cb.and(allPredicates.toArray(Predicate[]::new)),
							cb.and(fullDesc.toArray(Predicate[]::new))
			);
			finalPredicate = cb.and(
							descriptionPred, cityPred
			);
		} else {
			params.put("query", "%" + sqlQuery.toLowerCase(Locale.ROOT) + "%");
			System.out.println("query is: " + params.get("query").toString());
			predicates.add(cb.like(cb.lower(event.get("briefDescription")), String.valueOf(params.get("query"))));
			predicates.add(cb.like(cb.lower(event.get("fullDescription")), String.valueOf(params.get("query"))));
			descriptionPred = cb.or(
							cb.and(predicates.toArray(Predicate[]::new)),
							cb.or(predicates.toArray(Predicate[]::new))
			);
			finalPredicate = cb.and(descriptionPred, cityPred);
		}

		//add orders to query
		List<Order> orders = new ArrayList<>();

		if (!(params.get("dateSort") == null)) {
			if (Boolean.parseBoolean(String.valueOf(params.get("dateSort"))))
				orders.add(cb.asc(event.get("dateTime")));
			else
				orders.add(cb.desc(event.get("dateTime")));
		}

		query.select(event)
//			.where(cb.or(briefDesc,fullDesc));
						.where(finalPredicate)
						.orderBy(orders);

		Query<Event> query1 = session.createQuery(query);

		return query1.getResultList();
	}
}
