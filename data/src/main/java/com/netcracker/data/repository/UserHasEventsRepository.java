package com.netcracker.data.repository;

import com.netcracker.data.models.Event;
import com.netcracker.data.models.UserHasEvents;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserHasEventsRepository extends JpaRepository<UserHasEvents, UUID>{
    @Query(value = "select u.* from users_has_events u where u.users_user_id=:userId",
            nativeQuery = true)
    List<UserHasEvents> getListUserHasEventsByUserId(@Param("userId") UUID userId);

    @Query(value = "select u.* from users_has_events u where u.users_user_id=:userId and u.events_event_id=:eventId",
          nativeQuery = true)
    UserHasEvents findByUserFromEvent(@Param("userId") UUID userId, @Param("eventId") UUID eventId);

    @Query(value = "select u.*, e.rating,e.members_count,e.date_time from users_has_events u join events e on e.event_id = u.events_event_id where u.users_user_id=:userId ",
            nativeQuery = true)
    Page<UserHasEvents> getListUserHasEventsByUserIdPageable(@Param("userId") UUID userId, Pageable pageable);
}


