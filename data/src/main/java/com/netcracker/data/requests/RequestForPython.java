package com.netcracker.data.requests;

import com.netcracker.data.dto.EventByReccomendationServiceDTO;
import com.netcracker.data.dto.UserEventsDTO;
import com.netcracker.data.dto.UserEventsItemsDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class RequestForPython {
//    private static final String urlPythonService = "http://localhost:5050";
//   private static final String urlPythonService = "http://localhost:5000";

	private static final String urlPythonService = "http://34.65.102.51:5050";


	public RequestForPython() {
	}

	public List<EventByReccomendationServiceDTO> getReccomendationForUser(List<UserEventsDTO> eventsDTOList) {
		RestTemplate restTemplate = new RestTemplate();
		if (eventsDTOList != null && eventsDTOList.size() > 0) {
			EventByReccomendationServiceDTO[] response = restTemplate.postForObject(urlPythonService + "/get_reccomend", eventsDTOList, EventByReccomendationServiceDTO[].class);
			return Arrays.asList(response);
		} else return null;
	}

	public void updateReccomendation(List<UserEventsDTO> eventsDTOList) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForLocation(urlPythonService + "/events", eventsDTOList, UserEventsDTO[].class);
	}

	public void updateAllReccomendation(List<UserEventsDTO> eventsDTOList) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForLocation(urlPythonService + "/update_all", eventsDTOList, UserEventsDTO[].class);
	}

	public void deleteReccomendation(List<UserEventsDTO> eventsDTOList) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForLocation(urlPythonService + "/events_del", eventsDTOList, UserEventsDTO[].class);
	}
}
