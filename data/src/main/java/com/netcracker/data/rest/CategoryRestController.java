package com.netcracker.data.rest;

import com.netcracker.data.dto.CategoriesEventDTO;
import com.netcracker.data.dto.CountryDTO;
import com.netcracker.data.models.CategoriesEvents;
import com.netcracker.data.models.Country;
import com.netcracker.data.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category/")
public class CategoryRestController {

  private final CategoryService categoryService;

  @Autowired
  public CategoryRestController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CategoriesEventDTO>> getCategories(){
    List<CategoriesEvents> categoriesEvents = this.categoryService.getAll();
    if (categoriesEvents.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(this.categoryService.toCategoriesEventDTO(categoriesEvents), HttpStatus.OK);
  }
}
