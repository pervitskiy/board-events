package com.netcracker.data.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.netcracker.data.dto.CityDTO;
import com.netcracker.data.dto.CountryDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.mapStruct.CityMapper;
import com.netcracker.data.mapStruct.CountryMapper;
import com.netcracker.data.models.City;
import com.netcracker.data.models.Country;
import com.netcracker.data.service.CityService;
import com.netcracker.data.service.CountryService;
import com.netcracker.data.transfer.AdminDetails;
import com.netcracker.data.transfer.Details;
import com.netcracker.data.transfer.New;
import com.netcracker.data.transfer.UpdateName;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/countries/")
public class CountryRestController {
  private CountryMapper countryMapper;
  private CountryService countryService;
  private CityService cityService;
  private CityMapper cityMapper;

  @Autowired
  public CountryRestController(CountryMapper countryMapper, CityMapper cityMapper, CountryService countryService, CityService cityService) {
    this.countryMapper = countryMapper;
    this.countryService = countryService;
    this.cityService = cityService;
    this.cityMapper = cityMapper;
  }

  @JsonView(AdminDetails.class)
  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CountryDTO> getCountry(@PathVariable("id") UUID countryId){
    if (countryId == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Country country = null;
    try {
      country = this.countryService.getById(countryId);
    } catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(this.countryService.toCountryDTO(country), HttpStatus.OK);
  }

  @PostMapping(value = "{id}/cities", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CityDTO>> getListCityByCountry(@PathVariable("id") UUID countryId, @RequestBody CityDTO charCity){
    if (countryId == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Country country = null;
    try {
      country = this.countryService.getById(countryId);
    } catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    List<City> cityList = null;
    if (charCity.getTitleRu() != null) {
       cityList = this.cityService.getListCity(country.getCountryId(), charCity.getTitleRu());
    }
    else {
      cityList = this.cityService.getDefaultListCity(country.getCountryId());
    }
    return new ResponseEntity(this.cityService.toCityDTOs(cityList), HttpStatus.OK);
  }

  @JsonView(AdminDetails.class)
  @PostMapping(value = "", consumes = "application/json")
  public ResponseEntity<CountryDTO> saveCountry(@Validated(New.class) @RequestBody CountryDTO countryDTO){
    if (countryDTO == null){
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Country newCountry = this.countryService.save(countryDTO);
    return new ResponseEntity<>(this.countryService.toCountryDTO(newCountry), HttpStatus.CREATED);
  }

  @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CountryDTO> updateCountry(@PathVariable("id") UUID countryId, @Validated(UpdateName.class) @RequestBody CountryDTO newCountryDTO){
    if (countryId == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    if (newCountryDTO == null){
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Country country = null;
    try {
      country = this.countryService.getById(countryId);
    } catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(this.countryService.toCountryDTO(this.countryService.update(country, newCountryDTO)), HttpStatus.OK);
  }

  @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity deleteCountry(@Validated @PathVariable("id") UUID countryId){
    if (countryId == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Country country = null;
    try {
      country = this.countryService.getById(countryId);
    } catch (EntityNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);        }
    this.countryService.delete(countryId);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @JsonView(Details.class)
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CountryDTO>> getAllCountry(){
    List<Country> countries = this.countryService.getAll();
    if (countries.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(countryMapper.toCountryDTOs(countries), HttpStatus.OK);
  }
}
