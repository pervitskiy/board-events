package com.netcracker.data.rest;

import com.netcracker.data.criteria.EventSearchCriteria;
import com.netcracker.data.criteria.EventSpecifications;
import com.netcracker.data.criteria.service.EventCriteriaService;
import com.netcracker.data.dto.*;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.City;
import com.netcracker.data.models.Event;
import com.netcracker.data.models.User;
import com.netcracker.data.service.EventService;
import com.netcracker.data.service.UserHasEventsService;
import com.netcracker.data.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/event/")
public class EventRestController {

	static final Logger logger = LoggerFactory.getLogger(EventRestController.class);

	private final EventService eventService;
	private final EventCriteriaService eventCriteriaService;

	/**
	 * Нужно чекать есть ли пользователь в бд
	 **/
	private final UserService userService;

	private final UserHasEventsService userHasEventsService;

	@Autowired
	public EventRestController(EventService eventService, UserService userService, EventCriteriaService eventSearchCriteria,
														 UserHasEventsService userHasEventsService) {
		this.eventCriteriaService = eventSearchCriteria;
		this.eventService = eventService;
		this.userService = userService;
		this.userHasEventsService = userHasEventsService;
	}

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> saveEvent(@RequestBody EventDTO eventDTO) throws DataIntegrityViolationException, EntityNotFoundException {
		if (eventDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		EventDTO event = this.eventService.save(eventDTO);
		return new ResponseEntity<>(event, HttpStatus.CREATED);
	}

	@GetMapping(value = "{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> getEvent(@PathVariable("eventId") UUID eventId) {
		if (eventId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Event event = null;
		try {
			event = eventService.getById(eventId);
		} catch (EntityNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(eventService.toEventDTO(event), HttpStatus.OK);
	}

	@GetMapping(value = "/all")
	public ResponseEntity<Integer> getEventsCount() {
		return new ResponseEntity<>(eventService.getEventsCount(), HttpStatus.OK);
	}

	@GetMapping(value = "/search")
	public ResponseEntity<Map<String, Object>> sortedEvents(
					@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date date,
					@RequestParam String query,
					@RequestParam(required = false) boolean dateSort,
					@RequestParam(required = false) boolean ratingSort,
					@RequestParam(required = false) boolean membersCountSort,
					@RequestParam(required = false) UUID cityUuid
	) {
		Map<String, Object> params = new HashMap<>();
		params.put("date", date);
		params.put("city", cityUuid);
		params.put("query", query);
		params.put("dateSort", dateSort);
		params.put("ratingSort", ratingSort);
		params.put("membersCountSort", membersCountSort);

		Map<String, Object> resp = new HashMap<>();

		List<EventDTO> sort = new ArrayList<>(eventService.getSearchedEvents(params));
		resp.put("events", sort);
		resp.put("totalItems", sort.size());

		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@GetMapping(path = "")
	public ResponseEntity<Map<String, Object>> getAllEvents(

					@RequestParam String query,

					@RequestParam(required = false)
									Optional<UUID> cityUuid,

					@RequestParam(required = false)
									Optional<UUID> countryUuid,

					@RequestParam(required = false)
									Optional<Boolean> dateSort,

					@RequestParam(required = false)
									Optional<Boolean> memberSort,

					@RequestParam(required = false)
									Optional<Boolean> ratingSort,

					@RequestParam(required = false)
									Optional<Double> ratingHigherThan,

					@RequestParam(required = false)
									Optional<Double> ratingLowerThan,

					@RequestParam(required = false)
									Optional<Integer> membersHigherThan,

					@RequestParam(required = false)
									Optional<Integer> membersLowerThan,

					@RequestParam(required = false)
									List<String> categories,

					@RequestParam(required = false)
					@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Optional<Date> dateEarlierThan,

					@RequestParam(required = false)
					@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Optional<Date> dateLaterThan,

					@RequestParam(defaultValue = "0") int page,
					@RequestParam(defaultValue = "10") int size

	) {

		logger.info(String.valueOf(categories));
		logger.info("categories");

//		Map<String, Boolean> sortParams = new HashMap<>();
		List<Sort.Order> orderList = new ArrayList<>();

		if (ratingSort.isPresent()) {
			if (ratingSort.get())
				orderList.add(new Sort.Order(Sort.Direction.DESC, "rating"));
			else
				orderList.add(new Sort.Order(Sort.Direction.ASC, "rating"));
		}
		if (dateSort.isPresent())
			if (dateSort.get())
				orderList.add(new Sort.Order(Sort.Direction.ASC, "dateTime"));
			else
				orderList.add(new Sort.Order(Sort.Direction.DESC, "dateTime"));
		if (memberSort.isPresent())
			if (memberSort.get())
				orderList.add(new Sort.Order(Sort.Direction.DESC, "membersCount"));
			else
				orderList.add(new Sort.Order(Sort.Direction.ASC, "membersCount"));

		logger.debug(query);
		logger.debug("query");

		Sort sortArray = Sort.by(orderList);
		Pageable pageable = PageRequest.of(page, size, sortArray);

		Integer cityId;
		cityId = cityUuid.map(uuid -> eventService.getCityByUUid(uuid).getCityId()).orElse(null);

		Integer countryId;
		countryId = countryUuid.map(uuid -> eventService.getCountryByUUid(uuid).getCountryId()).orElse(null);

//		Set<String> categoryName = null;
//		for (CategoriesEventDTO category: categories){
//			categoryName.add(category.getNameCategory());
//		}

		EventSearchCriteria criteria = EventSearchCriteria.builder()
						.query(Optional.ofNullable(query))
						.membersHigherThan(membersHigherThan)
						.membersLowerThan(membersLowerThan)
						.ratingHigherThan(ratingHigherThan)
						.ratingLowerThan(ratingLowerThan)
						.categoriesName(categories)
						.dateLaterThan(dateLaterThan)
						.dateEarlierThan(dateEarlierThan)
						.cityUuid(Optional.ofNullable(cityId))
						.countryUuid(Optional.ofNullable(countryId))
						.build();

		Page<Event> eventPage = eventCriteriaService.retrieveEvents(criteria, pageable);
		List<EventDTO> sort = eventService.toEventDTOS(eventPage);

		Map<String, Object> resp = new HashMap<>();
		resp.put("events", sort);
		resp.put("currentPage", eventPage.getNumber());
		resp.put("totalItems", eventPage.getTotalElements());
		resp.put("totalPages", eventPage.getTotalPages());


		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@GetMapping(path = "/old")
	public ResponseEntity<Map<String, Object>> getAllEvents(
					@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date date,
					@RequestParam(required = false) String query,
					@RequestParam(defaultValue = "0") int page,
					@RequestParam(defaultValue = "10") int size
	) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by("date_time"));
			//collect booleans in list and add all to Sort object for Pageable

			Page<Event> eventDTOList;
			if (date == null) {
				pageable = PageRequest.of(page, size, Sort.by("date_time"));
				eventDTOList = eventService.getAllEvents(query, pageable);
			} else {
				pageable = PageRequest.of(page, size, Sort.by("dateTime"));
				eventDTOList = eventService.getEventsAfter(date, query, pageable);
			}

			Map<String, Object> resp = new HashMap<>();
			List<EventDTO> sort = eventService.toEventDTOS(eventDTOList);

			resp.put("events", sort);
			resp.put("currentPage", eventDTOList.getNumber());
			resp.put("totalItems", eventDTOList.getTotalElements());
			resp.put("totalPages", eventDTOList.getTotalPages());

			return !eventDTOList.isEmpty()
							? new ResponseEntity<>(resp, HttpStatus.OK)
							: new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(value = "{eventId}/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> updateEvent(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.update(eventDTO, event), HttpStatus.CREATED);
	}

	@PatchMapping(value = "{eventId}/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> patchEvent(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.patchUser(eventDTO, event), HttpStatus.CREATED);
	}

	@PatchMapping(value = "{eventId}/comment", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> patchEventComment(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.patchComment(eventDTO, event), HttpStatus.CREATED);
	}

	@PatchMapping(value = "{eventId}/rating", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> patchEventRating(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.patchRating(eventDTO, event), HttpStatus.CREATED);
	}

	@PatchMapping(value = "{eventId}/like", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> patchEventLike(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.patchLike(eventDTO, event), HttpStatus.CREATED);
	}

	@PatchMapping(value = "{eventId}/favorites", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EventDTO> patchEventFavorites(@PathVariable("eventId") UUID eventId, @RequestBody EventDTO eventDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (eventId == null || eventDTO == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Event event = eventService.getById(eventId);
		return new ResponseEntity<>(eventService.patchFavorites(eventDTO, event), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<User> deletedEvent(@PathVariable("id") UUID eventId) throws EntityNotFoundException {
		if (eventId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Event event = eventService.getById(eventId);

		this.eventService.delete(event);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Передаем id user для получения рекомендаций
	 **/
	@GetMapping(value = "recommendation/{id}")
	public ResponseEntity<List<EventDTO>> getRecommendationEvent(@PathVariable("id") UUID userId) throws EntityNotFoundException {
		if (userId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		userService.getById(userId);
		List<EventDTO> recomendEvent = userHasEventsService.getRecommendation(userId);
		if (recomendEvent == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(recomendEvent, HttpStatus.OK);
	}

	@GetMapping(value = "recommendation2/{id}")
	public ResponseEntity<List<UserEventsDTO>> getRecommendationEvent2(@PathVariable("id") UUID userId) throws EntityNotFoundException {
		if (userId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		userService.getById(userId);//Нет пользователя, выкинет ошибку
		List<UserEventsDTO> recomendEvent = userHasEventsService.getRecommendation2(userId);
		if (recomendEvent == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(recomendEvent, HttpStatus.OK);
	}

	@PostMapping(value = "updateAllRecommendation")
	public ResponseEntity<HttpStatus> updateAllRecommendation() throws EntityNotFoundException {
		eventService.updateAllRecommendation();
		return new ResponseEntity<>(HttpStatus.OK);
	}

//  @PostMapping(value = "updateAllRecommendation2")
//  public ResponseEntity<List<UserEventsDTO>> getRecommendationEvent2() throws EntityNotFoundException {
//    eventService.updateAllRecommendation2();
//    return new ResponseEntity<>(eventService.updateAllRecommendation2(), HttpStatus.OK);
//  }

	@PutMapping(value = "{id}/delete/user")
	public ResponseEntity<User> deletedUserFromEvent(@PathVariable("id") UUID eventId, @RequestBody UserDTO user) throws EntityNotFoundException {
		if (eventId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
//		Event eventToSave = event;
//		eventToSave.deleteFromMembersCount();
//		eventToSave.setMembersCount(eventToSave.getMembersCount()-1);
//		this.eventService.save(eventService.toEventDTO(event));
		Event event1 = eventService.getById(eventId);
		this.eventService.deleteUser(event1, user);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping(value = "{id}/delete/like")
	public ResponseEntity<User> deletedLikeFromEvent(@PathVariable("id") UUID eventId, @RequestBody UserDTO user) throws EntityNotFoundException {
		if (eventId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Event event = eventService.getById(eventId);
		this.eventService.deleteLike(event, user);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping(value = "{id}/delete/favorites")
	public ResponseEntity<User> deletedFavoritesFromEvent(@PathVariable("id") UUID eventId, @RequestBody UserDTO user) throws EntityNotFoundException {
		if (eventId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Event event = eventService.getById(eventId);
		this.eventService.deleteFavorites(event, user);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping(value = "/delete/comment/{id}")
	public ResponseEntity<User> deletedCommentFromEvent(@PathVariable("id") UUID commentId) throws EntityNotFoundException {
		if (commentId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		this.eventService.deleteComment(commentId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}


}
