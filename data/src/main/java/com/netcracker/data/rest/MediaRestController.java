package com.netcracker.data.rest;

import com.netcracker.data.models.Media;
import com.netcracker.data.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/media/")
public class MediaRestController {

  @Autowired
  private MediaService mediaService;

  @PostMapping(value = "", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Media> saveMedia(@RequestBody Media media) {
    HttpHeaders headers = new HttpHeaders();
    if (media == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    this.mediaService.save(media);
    return new ResponseEntity<>(media, headers, HttpStatus.CREATED);
  }

  @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity deleteCountry(@PathVariable("id") UUID mediaID){
    if (mediaID == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    this.mediaService.delete(mediaID);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
