package com.netcracker.data.rest;

import com.netcracker.data.dto.AuthInfoDto;
import com.netcracker.data.dto.UserDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.models.User;
import com.netcracker.data.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/security")
@Slf4j
public class SecurityController {
    private static Logger logger = LoggerFactory.getLogger(SecurityController.class);
    private UserService userService;

    public SecurityController(UserService userService) {
        this.userService = userService;
    }

//    /**
//     * метод регистрации новых пользователей
//     * @param registerUserDto - dto для регистрации
//     * @return Status
//     */
//    @RequestMapping(method = POST, path = "/register")
//    @ResponseStatus(HttpStatus.CREATED)
//    public ResponseEntity<UserDTO> registerNewUser(@RequestBody UserDTO registerUserDto) {
//        UserDTO userDTO = userService.save(registerUserDto);
//        return new ResponseEntity<UserDTO>(registerUserDto, HttpStatus.CREATED);
//
//    }
    /**
     * метод Аутентификация  для пользоватлея
     * @param loginUserDto - dto для выхода
     * @return AuthInfoDto - где хранится токен для пользователя
     */
    @RequestMapping(method = POST, path = "/login")
    public ResponseEntity<AuthInfoDto> loginUser(@RequestBody UserDTO loginUserDto) throws EntityNotFoundException {
        AuthInfoDto authInfoDto = userService.loginUser(loginUserDto);
        return authInfoDto != null
                ? new ResponseEntity<AuthInfoDto>(authInfoDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
