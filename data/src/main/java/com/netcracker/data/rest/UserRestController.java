package com.netcracker.data.rest;

import com.netcracker.data.dto.*;
import com.netcracker.data.exception.EmailAlreadyInUseException;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.exception.UsernameAlreadyInUseException;
import com.netcracker.data.models.*;
import com.netcracker.data.service.EventService;
import com.netcracker.data.service.UserHasEventsService;
import com.netcracker.data.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/user/")
public class UserRestController {

	@Autowired
	private UserService userService;

	@Autowired
	private EventService eventService;

	@Autowired
	private UserHasEventsService userHasEventsService;

	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getAllUser() {
		List<UserDTO> userDTOS = userService.getAll();
		if (userDTOS != null && !userDTOS.isEmpty()) {
			return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping(value = "{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> getUser(@Valid @PathVariable("userId") UUID userId) throws EntityNotFoundException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		return new ResponseEntity<>(this.userService.toUserDTO(user), HttpStatus.OK);
	}

	@PostMapping(value = "", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO) throws EntityNotFoundException, DataIntegrityViolationException, UsernameAlreadyInUseException, EmailAlreadyInUseException {
		if (userDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		UserDTO user = this.userService.save(userDTO);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@PostMapping(value = "{id}/premium", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> postPremium(@PathVariable("id") UUID userId, @RequestBody PaymentInformationDTO paymentInformationDTO) throws EntityNotFoundException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (paymentInformationDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		return new ResponseEntity<>(userService.postPrem(user, paymentInformationDTO), HttpStatus.OK);
	}

	@PatchMapping(value = "{id}", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> patchUser(@PathVariable("id") UUID userId, @RequestBody UserDTO newUserDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (newUserDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		return new ResponseEntity<>(userService.patchUpdate(user, newUserDTO), HttpStatus.OK);
	}

	@PutMapping(value = "{id}", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> updateUser(@PathVariable("id") UUID userId, @RequestBody UserDTO newUserDTO) throws EntityNotFoundException, DataIntegrityViolationException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (newUserDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		UserDTO user = this.userService.update(this.userService.getById(userId), newUserDTO);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> deletedUser(@PathVariable("id") UUID userId) throws EntityNotFoundException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		this.userService.delete(user);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "{id}/events")
	public ResponseEntity<Map<String, Object>> getListUserEvents(
					@PathVariable("id")
									UUID userId,

					@RequestParam(required = false)
									Optional<Boolean> dateSort,

					@RequestParam(required = false)
									Optional<Boolean> memberSort,

					@RequestParam(required = false)
									Optional<Boolean> ratingSort,
					@RequestParam(defaultValue = "0") int page,
					@RequestParam(defaultValue = "10") int size
	) throws EntityNotFoundException {

		try {
			if (userId == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			User user = this.userService.getById(userId);

			List<Sort.Order> orderList = new ArrayList<>();

			if (ratingSort.isPresent()) {
				if (ratingSort.get())
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.rating"));
				else
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.rating"));
			}
			if (dateSort.isPresent())
				if (dateSort.get())
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.date_time"));
				else
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.date_time"));
			if (memberSort.isPresent())
				if (memberSort.get())
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.members_count"));
				else
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.members_count"));

			Sort sortArray = Sort.by(orderList);
			Pageable pageable = PageRequest.of(page, size, sortArray);

			Map<String, Object> resp = new HashMap<>();
			Page<UserHasEvents> result = userHasEventsService.getListUserHasEventsPageable(user.getUserID(), pageable);
			List<Event> userEvents = new ArrayList<>();
			for (UserHasEvents event : result) {
				userEvents.add(event.getEvent());
			}
			List<EventDTO> events = eventService.toEventDTOS(userEvents);

			resp.put("events", events);
			resp.put("totalItems", result.getTotalElements());
			resp.put("currentPage", result.getNumber());
			resp.put("totalPages", result.getTotalPages());

			return new ResponseEntity<>(resp, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "{id}/favourites")
	public ResponseEntity<Map<String, Object>> getListUserFavourites(
					@PathVariable("id") UUID userId,

					@RequestParam(required = false)
									Optional<Boolean> dateSort,

					@RequestParam(required = false)
									Optional<Boolean> memberSort,

					@RequestParam(required = false)
									Optional<Boolean> ratingSort,
					@RequestParam(defaultValue = "0") int page,
					@RequestParam(defaultValue = "10") int size

	) throws EntityNotFoundException {
		try {
			if (userId == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			User user = this.userService.getById(userId);

			List<Sort.Order> orderList = new ArrayList<>();

			if (ratingSort.isPresent()) {
				if (ratingSort.get())
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.rating"));
				else
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.rating"));
			}
			if (dateSort.isPresent())
				if (dateSort.get())
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.date_time"));
				else
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.date_time"));
			if (memberSort.isPresent())
				if (memberSort.get())
					orderList.add(new Sort.Order(Sort.Direction.DESC, "e.members_count"));
				else
					orderList.add(new Sort.Order(Sort.Direction.ASC, "e.members_count"));

			Sort sortArray = Sort.by(orderList);
			Pageable pageable = PageRequest.of(page, size, sortArray);

			Map<String, Object> resp = new HashMap<>();
			Page<FavoritesUserOnEvent> result = userService.getListFavouritesUserOnEventPageable(user.getUserID(), pageable);
			List<Event> userEvents = new ArrayList<>();
			for (FavoritesUserOnEvent event : result) {
				userEvents.add(event.getEvent());
			}
			List<EventDTO> events = eventService.toEventDTOS(userEvents);

			resp.put("events", events);
			resp.put("totalItems", result.getTotalElements());
			resp.put("currentPage", result.getNumber());
			resp.put("totalPages", result.getTotalPages());

			return new ResponseEntity<>(resp, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PatchMapping(value = "{id}/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> addCategories(@PathVariable("id") UUID userId, @RequestBody List<CategoriesEventDTO> categoriesEvents) throws EntityNotFoundException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		return new ResponseEntity<>(userService.addCategories(user, categoriesEvents), HttpStatus.OK);
	}

	@GetMapping(value = "{id}/categories")
	public ResponseEntity<List<CategoriesEventDTO>> getListCategories(@PathVariable("id") UUID userId) throws EntityNotFoundException {
		if (userId == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		User user = this.userService.getById(userId);
		List<CategoriesEventDTO> categoriesEventDTOS = userService.getListCategories(user);
		if (categoriesEventDTOS != null && !categoriesEventDTOS.isEmpty())
			return new ResponseEntity<>(userService.getListCategories(user), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "{id}/countCreateEvent")
	public ResponseEntity<Integer> getCountOrganizerEventLastMonth(@PathVariable("id") UUID userId) throws EntityNotFoundException {
		User user = this.userService.getById(userId);
		return ResponseEntity.ok(this.userService.getCountOrganizerEventLastMonth(userId));
	}


}
