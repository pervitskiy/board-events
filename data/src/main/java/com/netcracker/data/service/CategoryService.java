package com.netcracker.data.service;

import com.netcracker.data.dto.CategoriesEventDTO;
import com.netcracker.data.mapStruct.CategoriesMapper;
import com.netcracker.data.models.CategoriesEvents;
import com.netcracker.data.repository.CategoryEventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
  private final CategoryEventsRepository categoryEventsRepository;
  private final CategoriesMapper categoriesMapper;

  @Autowired
  public CategoryService(CategoryEventsRepository categoryEventsRepository,
                         CategoriesMapper categoriesMapper) {
    this.categoryEventsRepository = categoryEventsRepository;
    this.categoriesMapper = categoriesMapper;
  }

  public List<CategoriesEvents> getAll() {
    return categoryEventsRepository.findAll();
  }

  public List<CategoriesEventDTO> toCategoriesEventDTO(List<CategoriesEvents> categoriesEvents) {
    return categoriesMapper.toCategoriesDTOsFromCategories(categoriesEvents);
  }
}
