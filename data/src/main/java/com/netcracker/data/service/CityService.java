package com.netcracker.data.service;

import com.netcracker.data.dto.CityDTO;
import com.netcracker.data.mapStruct.CityMapper;
import com.netcracker.data.models.City;
import com.netcracker.data.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {
    CityRepository cityRepository;
    CityMapper cityMapper;

    @Autowired
    public CityService(CityRepository cityRepository,
                       CityMapper cityMapper) {
        this.cityRepository = cityRepository;
        this.cityMapper = cityMapper;
    }

    public List<City> getListCity(Integer id, String str){
        return cityRepository.getListCityByCountryId(id, str.length(), str);
    }

    public List<CityDTO> toCityDTOs(List<City> cities){
        return cityMapper.toCityDTOs(cities);
    }

  public List<City> getDefaultListCity(Integer countryId) {
    return cityRepository.getListDefaultCityByCountryId(countryId);
  }
}
