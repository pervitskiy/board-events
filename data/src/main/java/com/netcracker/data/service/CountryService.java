package com.netcracker.data.service;


import com.netcracker.data.dto.CountryDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.mapStruct.CountryMapper;
import com.netcracker.data.models.Country;
import com.netcracker.data.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CountryService {
    private CountryRepository countryRepository;
    private CountryMapper countryMapper;

    @Autowired
    public CountryService(CountryRepository countryRepository, CountryMapper countryMapper) {
        this.countryRepository = countryRepository;
        this.countryMapper = countryMapper;
    }

    public Country getById(UUID id) throws EntityNotFoundException {
        return countryRepository.findById(id).orElseThrow(()-> new EntityNotFoundException(id, "Country"));
    }

    public Country save(CountryDTO countryDTO){
        return countryRepository.save(countryMapper.toCountry(countryDTO));
    }

    public CountryDTO toCountryDTO (Country country){
        return countryMapper.toCountryDTO(country);
    }

    public CountryDTO getByUpdateCountryDTO(CountryDTO countryDTO){
        Country country = countryRepository.findById(countryMapper.toCountry(countryDTO).getCountryUuidId()).get();
        return countryMapper.toCountryDTO(country);
    }

    public Country update(Country country, CountryDTO newCountryDTO){
        //меняем только русский язык
        country.setTitleRu(newCountryDTO.getCountryName());
        return countryRepository.save(country);
    }

    public void delete(UUID id){
        countryRepository.deleteById(id);
    }

    public List<Country> getAll(){
        return countryRepository.findAllByOrderByTitleRu();
    }




}
