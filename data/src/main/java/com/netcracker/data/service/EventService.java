package com.netcracker.data.service;

import com.netcracker.data.criteria.EventSearchCriteria;
import com.netcracker.data.criteria.EventSpecifications;
import com.netcracker.data.criteria.service.EventCriteriaService;
import com.netcracker.data.dto.CityDTO;
import com.netcracker.data.dto.EventDTO;
import com.netcracker.data.dto.UserDTO;
import com.netcracker.data.dto.UserEventsDTO;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.mapStruct.CityMapper;
import com.netcracker.data.mapStruct.EventMapper;
import com.netcracker.data.models.*;
import com.netcracker.data.repository.*;
import com.netcracker.data.requests.RequestForPython;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class EventService implements EventCriteriaService {

	Logger logger = LoggerFactory.getLogger(EventService.class);

	private EventsRepository eventsRepository;
	private CategoryEventsRepository categoryEventsRepository;
	private UserHasEventsRepository userHasEventsRepository;
	private LikeUserOnEventRepository likeUserOnEventRepository;
	private FavoritesUserOnEventRepository favoritesUserOnEventRepository;
	private RatingRepository ratingRepository;
	private CommentsRepository commentsRepository;
	private RequestForPython requestForPython;
	private EventMapper eventMapper;
	private CityRepository cityRepository;
	private CountryRepository countryRepository;
	private CityMapper cityMapper;

	@Autowired
	public EventService(EventsRepository eventsRepository, CategoryEventsRepository categoryEventsRepository, RatingRepository ratingRepository, CommentsRepository commentsRepository, UserHasEventsRepository userHasEventsRepository, LikeUserOnEventRepository likeUserOnEventRepository, FavoritesUserOnEventRepository favoritesUserOnEventRepository, RequestForPython requestForPython, EventMapper eventMapper, CityRepository cityRepository, CountryRepository countryRepository, CityMapper cityMapper) {
		this.eventsRepository = eventsRepository;
		this.countryRepository = countryRepository;
		this.categoryEventsRepository = categoryEventsRepository;
		this.cityRepository = cityRepository;
		this.ratingRepository = ratingRepository;
		this.cityMapper = cityMapper;
		this.commentsRepository = commentsRepository;
		this.requestForPython = requestForPython;
		this.userHasEventsRepository = userHasEventsRepository;
		this.likeUserOnEventRepository = likeUserOnEventRepository;
		this.favoritesUserOnEventRepository = favoritesUserOnEventRepository;
		this.eventMapper = eventMapper;
	}

	public Event getById(UUID uuid) throws EntityNotFoundException {
		return eventsRepository.findById(uuid).orElseThrow(() -> new EntityNotFoundException(uuid, "Event"));
	}

	public EventDTO toEventDTO(Event event) {
		return eventMapper.toEventDTO(event);
	}

	public EventDTO save(EventDTO eventDTO) throws EntityNotFoundException {
		Event event = eventMapper.toEvent(eventDTO);
		Event newEvent = eventsRepository.save(event);
		//обновляем рекомендации (теперь в шедуллере это делается)
		//List<Event> list = new ArrayList<Event>();
		//list.add(newEvent);
		//requestForPython.updateReccomendation(eventMapper.toUserEventsDTOs(list));
		return toEventDTO(newEvent);
	}

	public EventDTO update(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		return toEventDTO(eventsRepository.save(eventMapper.updateEventFromEventDto(eventDTO, event)));
	}

	public EventDTO patchUser(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		return toEventDTO(eventsRepository.save(eventMapper.patchEventFromEventDto(eventDTO, event)));
	}

	public List<EventDTO> toEventDTOS(Page<Event> eventPage) {
		return eventMapper.toEventDTOs(eventPage.getContent());
	}

	public List<EventDTO> toEventDTOS(List<Event> eventPage) {
		return eventMapper.toEventDTOs(eventPage);
	}

	public City getCityByUUid(UUID cityUuid) {
		return cityRepository.getOne(cityUuid);
	}

	public Country getCountryByUUid(UUID countryUuid) {
		return countryRepository.getOne(countryUuid);
	}

	public Page<Event> getAllEvents(String query, Pageable pageable) {
		if (query != null)
			return eventsRepository.querySearch(query, pageable);
		else
			return eventsRepository.findAll(pageable);
	}

	public List<EventDTO> getSearchedEvents(Map<String, Object> params) {
		return eventMapper.toEventDTOs(eventsRepository.findEventsByParams(params));
	}

	public Page<Event> getEventsAfter(Date date, String query, @PageableDefault(sort = {"dateTime"}) Pageable pageable) {
		if (query != null)
			return eventsRepository.findByDateTimeAfter(date, query, pageable);
		else
			return eventsRepository.findByDateTimeAfter(date, pageable);
	}

	@Transactional()
	@Override
	public Page<Event> retrieveEvents(EventSearchCriteria criteria, Pageable pageable) {
		List<Integer> categories = new ArrayList<>();
		if (criteria.getCategoriesName() != null) {
			for (String categoryName : criteria.getCategoriesName()) {
				logger.info("current category name"+categoryName);
				Integer current = categoryEventsRepository.findByNameCategory(categoryName).get().getCategoryEventID();
				logger.info("added"+String.valueOf(current));
				if (current != null)
					categories.add(current);
			}
			criteria.setCategories(categories);
		}
		Specification<Event> eventSpecification = EventSpecifications.createEventSpecifications(criteria);
		return eventsRepository.findAll(eventSpecification, pageable);
	}

	@Override
	public Optional<Event> retrieveEvent(UUID uuid) {
		return Optional.empty();
	}

	public Integer getEventsCount() {
		return eventsRepository.findAll().size();
	}

	public void delete(Event event) {
		eventsRepository.delete(event);
	}

	public void deleteUser(Event event, UserDTO user) {
		if (event == null) {
			System.out.println("event is null!");
		}
		event.setMembersCount(event.getMembersCount() - 1);
		eventsRepository.save(event);
		UserHasEvents userHasEvents = this.userHasEventsRepository.findByUserFromEvent(user.getUserId(), event.getEventId());
		this.userHasEventsRepository.delete(userHasEvents);
	}

	public void flush() {
		this.eventsRepository.flush();
	}

	public void saveEvent(Event event) {
		System.out.println(event);
		this.eventsRepository.flush();
	}

	public EventDTO patchComment(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		return toEventDTO(eventsRepository.save(eventMapper.patchEventCommentFromEventDto(eventDTO, event)));
	}

	public EventDTO patchRating(EventDTO eventDTO, Event event) throws EntityNotFoundException {
//		Event newEvent = eventMapper.patchEventRatingFromEventDto(eventDTO, event);
		eventsRepository.save(eventMapper.patchEventRatingFromEventDto(eventDTO, event));
//		int ratingsCount = ratingRepository.findRatingsByEventUuid(newEvent.getEventId());
//		EventDTO eventDTO1 = toEventDTO(newEvent);
//		eventDTO1.setRating(((eventDTO.getRating() * ratingsCount) + eventDTO.getRating()) / ratingsCount + 1);
//		update(eventDTO1,newEvent);
//		logger.debug(String.valueOf(ratingRepository.findRatingsByEventUuid(event.getEventId())));
		event.setRating(ratingRepository.findRatingsByEventUuid(event.getEventId()));
		eventsRepository.save(event);
		return toEventDTO(event);
	}

	public EventDTO patchLike(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		return toEventDTO(eventsRepository.save(eventMapper.patchEventLikeFromEventDto(eventDTO, event)));
	}

	public EventDTO patchFavorites(EventDTO eventDTO, Event event) throws EntityNotFoundException {
		return toEventDTO(eventsRepository.save(eventMapper.patchEventFavoritesFromEventDto(eventDTO, event)));
	}

	public void deleteLike(Event event, UserDTO user) {
		LikeUserOnEvent likeUserOnEvent = this.likeUserOnEventRepository.findByLikeFromEvent(user.getUserId(), event.getEventId());
		this.likeUserOnEventRepository.delete(likeUserOnEvent);
	}

	public void deleteFavorites(Event event, UserDTO user) {
		FavoritesUserOnEvent favoritesUserOnEvent = this.favoritesUserOnEventRepository.findByFavoriteFromEvent(user.getUserId(), event.getEventId());
		this.favoritesUserOnEventRepository.delete(favoritesUserOnEvent);
	}

	public void deleteComment(UUID commentId) {
		Comment comment = this.commentsRepository.findByCommentId(commentId);
		this.commentsRepository.delete(comment);
	}

//	public List<UserEventsDTO> getRecommendation(UUID uuid) {
//		List<UserEventsDTO> eventsDTOList = requestForPython.getReccomendationForUser(eventsRepository.findByUserId(uuid));
//		return eventsDTOList;
//	}

	public void updateAllRecommendation() {
		List<Event> events = eventsRepository.findAllEvents();
		logger.info(String.valueOf("event size is: "+events.size()));
		List<UserEventsDTO> userEventsDTOS = eventMapper.toUserEventsDTOs(events);
		logger.info(String.valueOf("userEventsDTOS size is: "+userEventsDTOS.size()));
		requestForPython.updateAllReccomendation(userEventsDTOS);
	}

	public List<UserEventsDTO> updateAllRecommendation2() {
		List<Event> events = eventsRepository.findAllEvents();
		List<UserEventsDTO> userEventsDTOS = eventMapper.toUserEventsDTOs(events);
		return userEventsDTOS;
	}
}
