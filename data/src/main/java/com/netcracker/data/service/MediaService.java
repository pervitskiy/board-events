package com.netcracker.data.service;

import com.netcracker.data.models.Media;
import com.netcracker.data.models.User;
import com.netcracker.data.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MediaService {

    MediaRepository mediaRepository;

    @Autowired
    public MediaService(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }

    //вернуть все объекты медиа, которые связаны с пользователем,
    //(и чтоб у этого объета не было ссылок на событие)
    public List<Media> getListMediaByUserId(UUID userId){
        return mediaRepository.getListMediaByUserId(userId);
    };

    public void save(Media media){
        mediaRepository.save(media);
    }

    public void update(List<Media> mediaList, User user){
        System.out.println("Длина медиа при обновлении:  " + mediaList.size());
        List<Media> oldMedia = mediaRepository.getListMediaByUserId(user.getUserID());
        for (int j=0; j<oldMedia.size(); j++){
            mediaRepository.delete(oldMedia.get(j));
        }
        System.out.println("Длина после чистки передаваемого списка медиа: " + mediaList.size());
        for (int i=0; i<mediaList.size(); i++){
            if (mediaList.get(i).getUser() == null){
                mediaList.get(i).setUser(user);
                mediaRepository.save(mediaList.get(i));
            }
        }
    }

    public void delete(UUID pk){
        mediaRepository.deleteById(pk);
    }

    public List<Media> getAll(){
        return mediaRepository.findAll();
    }

}
