package com.netcracker.data.service;

import com.netcracker.data.dto.EventByReccomendationServiceDTO;
import com.netcracker.data.dto.EventDTO;
import com.netcracker.data.dto.UserEventsDTO;
import com.netcracker.data.mapStruct.EventMapper;
import com.netcracker.data.models.Event;
import com.netcracker.data.models.UserHasEvents;
import com.netcracker.data.repository.EventsRepository;
import com.netcracker.data.repository.UserHasEventsRepository;
import com.netcracker.data.requests.RequestForPython;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserHasEventsService {
	public UserHasEventsService(UserHasEventsRepository userHasEventsRepository, EventsRepository eventsRepository, RequestForPython requestForPython, EventMapper eventMapper) {
		this.userHasEventsRepository = userHasEventsRepository;
		this.eventsRepository = eventsRepository;
		this.requestForPython = requestForPython;
		this.eventMapper = eventMapper;
	}

	private UserHasEventsRepository userHasEventsRepository;
	private EventsRepository eventsRepository;
	private RequestForPython requestForPython;
	private EventMapper eventMapper;

	//мж оптимизировать прийдется
	public List<EventDTO> getRecommendation(UUID uuid) {
		//находим все события, на которые подписывался пользователь
		List<UserHasEvents> userHasEvents = getListUserHasEvents(uuid);
		//получаем все события и отправляем их на анализ в рекомендации
		List<Event> events = new ArrayList<>();
		for (UserHasEvents userHasEvent : userHasEvents) {
			events.add(eventsRepository.findById(userHasEvent.getEvent().getEventId()).get());
		}
		List<EventByReccomendationServiceDTO> eventsReccomendRequest = requestForPython.getReccomendationForUser(
						eventMapper.toUserEventsDTOs(events)
		);
		List<Event> eventReccomend = new ArrayList<>();
		//получаем список рекомендаванных событий
		for (EventByReccomendationServiceDTO eventByReccomendationServiceDTO : eventsReccomendRequest) {
			try {
				Optional<Event> optionalEvent = eventsRepository.findById(eventByReccomendationServiceDTO.get_id());
				if (optionalEvent.isPresent()) {
					eventReccomend.add(optionalEvent.get());
				}
			} catch (NullPointerException exception) {
				System.err.println(exception.getMessage());
			}
		}
		List<EventDTO> eventDTOS = new ArrayList<>();
		for (Event event : eventReccomend) {
			if (event.getDateTime().getTime() > System.currentTimeMillis()) {
				eventDTOS.add(eventMapper.toEventDTO(event));
			}
		}
		return eventDTOS;
	}

	public List<UserEventsDTO> getRecommendation2(UUID uuid) {
		//находим все события, на которые подписывался пользователь
		List<UserHasEvents> userHasEvents = getListUserHasEvents(uuid);
		//получаем все события и отправляем их на анализ в рекомендации
		List<Event> events = new ArrayList<>();
		for (UserHasEvents userHasEvent : userHasEvents) {
			events.add(eventsRepository.findById(userHasEvent.getEvent().getEventId()).get());
		}
		return eventMapper.toUserEventsDTOs(events);
	}


	public List<UserHasEvents> getListUserHasEvents(UUID userId) {
		return this.userHasEventsRepository.getListUserHasEventsByUserId(userId);
	}

	public Page<UserHasEvents> getListUserHasEventsPageable(UUID userId, Pageable pageable) {
		return this.userHasEventsRepository.getListUserHasEventsByUserIdPageable(userId, pageable);
	}
}
