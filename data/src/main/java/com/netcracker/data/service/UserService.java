package com.netcracker.data.service;

import com.netcracker.data.dto.*;
import com.netcracker.data.exception.EmailAlreadyInUseException;
import com.netcracker.data.exception.EntityNotFoundException;
import com.netcracker.data.exception.FieldValidationError;
import com.netcracker.data.exception.UsernameAlreadyInUseException;
import com.netcracker.data.mapStruct.*;
import com.netcracker.data.models.*;
import com.netcracker.data.repository.*;
import com.netcracker.data.security.JwtSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.FieldPosition;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

	private UserRepository userRepository;
	private MediaRepository mediaRepository;
	private CategoryFileRepository categoryFileRepository;
	private CategoryEventsRepository categoryEventsRepository;
	private CategoriesHasUsersRepository categoriesHasUsersRepository;
	private CountryRepository countryRepository;
	private CityRepository cityRepository;
	private EventMapper eventMapper;
	private UserMapper userMapper;
	private CityMapper cityMapper;
	private UserEventsMapper userEventsMapper;
	private CategoriesMapper categoriesMapper;
	private PasswordEncoder passwordEncoder;
	private JwtSupplier jwtSupplier;
	private OrganizersHasEventsRepository organizersHasEventsRepository;
	private PaymentInformationMapper paymentInformationMapper;
	private FavoritesUserOnEventRepository favoritesUserOnEventRepository;

	@Autowired
	public UserService(MediaRepository mediaRepository, UserRepository userRepository,
										 UserMapper userMapper, CategoryFileRepository categoryFileRepository,
										 UserEventsMapper userEventsMapper,
										 CategoriesHasUsersRepository categoriesHasUsersRepository,
										 CategoryEventsRepository categoryEventsRepository,
										 CategoriesMapper categoriesMapper, CityRepository cityRepository,
										 CountryRepository countryRepository,
                     OrganizersHasEventsRepository organizersHasEventsRepository,
										 PasswordEncoder passwordEncoder, CityMapper cityMapper,
										 EventMapper eventMapper, JwtSupplier jwtSupplier,
                     PaymentInformationMapper paymentInformationMapper,
										 FavoritesUserOnEventRepository favoritesUserOnEventRepository) {
		this.mediaRepository = mediaRepository;
		this.favoritesUserOnEventRepository = favoritesUserOnEventRepository;
		this.organizersHasEventsRepository = organizersHasEventsRepository;
		this.eventMapper = eventMapper;
		this.userRepository = userRepository;
		this.userMapper = userMapper;
		this.cityMapper = cityMapper;
		this.cityRepository = cityRepository;
		this.categoryFileRepository = categoryFileRepository;
		this.userEventsMapper = userEventsMapper;
		this.categoriesHasUsersRepository = categoriesHasUsersRepository;
		this.categoryEventsRepository = categoryEventsRepository;
		this.categoriesMapper = categoriesMapper;
		this.countryRepository = countryRepository;
		this.passwordEncoder = passwordEncoder;
		this.jwtSupplier = jwtSupplier;
		this.paymentInformationMapper = paymentInformationMapper;
	}

	public User getById(UUID id) throws EntityNotFoundException {
		return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id, "User"));
	}

	public UserDTO toUserDTO(User user) {
		return userMapper.toUserDTO(user);
	}

	public List<EventDTO> getUserEventsDTO(User user) {
		List<Event> events = user.getUserHasEvents().stream().map(userHasEvents -> userHasEvents.getEvent()).collect(Collectors.toList());
		if (!events.isEmpty()) {
			return eventMapper.toEventDTOs(events);
		} else return null;
	}

  public List<EventDTO> getUserFavourites(User user){
		List<Event> events = user.getFavoritesUserOnEvents().stream().map(FavoritesUserOnEvent::getEvent).collect(Collectors.toList());
		if (!events.isEmpty()){
			return eventMapper.toEventDTOs(events);
		} else return null;
	}

	public AuthInfoDto loginUser(UserDTO userDTO) throws EntityNotFoundException {
		var user = userRepository.findByEmail(userDTO.getEmail());
		if (user == null)
      throw new EntityNotFoundException(userDTO.getEmail());
		if (passwordEncoder.matches(userDTO.getPassword(), user.getPassword())) {
			return generateTokenFromUser(user);
		}
		return null;
	}

	public UserDTO save(UserDTO userDTO) throws EntityNotFoundException, EmailAlreadyInUseException, UsernameAlreadyInUseException {
		var password = passwordEncoder.encode(userDTO.getPassword());
		userDTO.setPassword(password);
		User user = userMapper.toUser(userDTO);
		Country country;
		if (userRepository.findByEmail(userDTO.getEmail()) != null){
		  throw new EmailAlreadyInUseException(userDTO.getEmail());
    }

		if (userRepository.findByUsername(userDTO.getUsername()) != null){
      throw new UsernameAlreadyInUseException(userDTO.getUsername());
    }

		if (userDTO.getCountry() != null) {
			country = countryRepository.findByTitleRu(userDTO.getCountry().getCountryName());
			user.setCountry(country);
		}
		else {
			country = countryRepository.findByTitleRu("Россия");
			user.setCountry(country);
		}
		return userMapper.toUserDTO(userRepository.save(user));
	}

	//некорреткно сохраняется категория, но в методе putch все ок
	//он не делает set, а берет get и после прибовляет у него и как раз мы не делаем присвоение(
	//короче переписать надо. РУКАМи set!
	public UserDTO update(User user, UserDTO newUserDTO) throws EntityNotFoundException {
		User saveUser = userMapper.updateUserFromUserDtoMapper(newUserDTO, user);
		Country country;
		if (newUserDTO.getCountry() != null) {
			country = countryRepository.findByTitleRu(newUserDTO.getCountry().getCountryName());
			saveUser.setCountry(country);
		}
		return userMapper.toUserDTO(userRepository.save(saveUser));
	}

	public void delete(User user) {
		userRepository.delete(user);
	}

	public List<UserDTO> getAll() {
		return userMapper.toUserDTOs(userRepository.findAll());
	}

	public UserDTO patchUpdate(User user, UserDTO newUserDTO) throws EntityNotFoundException {
		User newUser = userMapper.toUser(newUserDTO);
//        user.setFirstName( newUser.getFirstName() );
//        user.setLastName( newUser.getLastName() );
//        user.setInformationAboutYourself( newUser.getInformationAboutYourself() );
//        user.setEmail( newUser.getEmail() );
//        var password = passwordEncoder.encode(newUserDTO.getPassword());
//        newUserDTO.setPassword(password);
//        user.setPassword( newUser.getPassword() );
		for (Media mediaDTO : newUser.getMedias()) {
			user.addMediaUser(mediaDTO);
		}
		//необходимо исключить дубликаты при добавлении(если вызвать этот метод после put, то все ок)
		for (CategoriesHasUsers categoriesHasUsers : newUser.getCategoriesHasUsers()) {
			user.addCategoriesHasUsers(categoriesHasUsers);
		}
		return userMapper.toUserDTO(userRepository.save(user));
	}

	public UserDTO addCategories(User user, List<CategoriesEventDTO> categoriesEventDTOS) throws EntityNotFoundException {
		for (CategoriesEventDTO categoriesEventDTO : categoriesEventDTOS) {
			user.addCategoriesHasUsers(categoriesMapper.toCategoriesHasUsers(categoriesEventDTO));
		}
		return userMapper.toUserDTO(userRepository.save(user));
	}

	public List<CategoriesEventDTO> getListCategories(User user) {
		return categoriesMapper.toCategoriesDTOs(user.getCategoriesHasUsers());
	}

	public AuthInfoDto generateTokenFromUser(User user) {
		return new AuthInfoDto(jwtSupplier.createTokenForUser(user.getFirstName(), user.getLastName(), user.getEmail()), user.getUserID());
	}

	public UserDTO postPrem(User user, PaymentInformationDTO paymentInformationDTO){
	  if (paymentInformationDTO.getStatus().toUpperCase().equals("COMPLETED")) {
      user.setPrem(true);
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.MONTH, 1);
      user.setLastDatePrem(cal.getTime());
    }// надо сделать прем на месяц
    PaymentInformation paymentInformation = paymentInformationMapper.toPaymentInformation(paymentInformationDTO);
	  user.addPaymentInformation(paymentInformation);
	  return toUserDTO(userRepository.save(user));
  }

  public Integer getCountOrganizerEventLastMonth(UUID uuid){
    return organizersHasEventsRepository.findCountEventsByUserUuid(uuid);
  }

	public Page<FavoritesUserOnEvent> getListFavouritesUserOnEventPageable(UUID userId, Pageable pageable) {
		return this.favoritesUserOnEventRepository.getListFavouritesUserOnEventPageable(userId, pageable);
	}
}
