package com.netcracker.migration;

import com.netcracker.data.DataConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@ComponentScan
@Import(DataConfiguration.class)
public class MigrationConfiguration {
}
