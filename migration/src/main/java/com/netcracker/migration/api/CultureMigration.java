package com.netcracker.migration.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.migration.importers.CultureImporter;
import com.netcracker.migration.jackson.models.NewEventDto;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class CultureMigration {

  ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

//  @Scheduled(cron = "0 0 0/8 * * ?")
//@Scheduled(fixedDelay = 1800000)
  @Scheduled(cron = "1 * * * * ?")
  public void main() {

    CultureImporter service = new CultureImporter();

    service.downloadEvents("15");

    List<NewEventDto> eventList = service.parseEvents(mapper);

    service.saveAllEvents(eventList);
  }
}
