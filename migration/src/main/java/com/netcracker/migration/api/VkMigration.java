package com.netcracker.migration.api;

import com.netcracker.migration.importers.VkImporter;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VkMigration {

  static final Logger logger = LoggerFactory.getLogger(VkMigration.class);

  VkImporter vkImporter = new VkImporter();

  @Scheduled(cron = "0 0 0/6 * * ?")
//  @Scheduled(fixedDelay = 199999999)
  public void main() {

    //VkApiClient obj
    HttpTransportClient httpClient = HttpTransportClient.getInstance();
    VkApiClient vk = new VkApiClient(httpClient);

    //instance of actor
    UserActor actor = new UserActor(633134925, System.getenv("ACCESS_TOKEN"));

    //list of main cities identifiers for all countries (78s) - main cities
    List<Integer> mainCitiesIds = vkImporter.getCityIds(vk,actor,false);

    //list of events identifiers for main cities (~7 min) 1000 events for 1 city
    List<Integer> eventIds = vkImporter.getEventIds(mainCitiesIds,vk,actor);

    logger.info("event_ids size final: " + eventIds.size());

    vkImporter.saveAllEvents(eventIds,vk,actor);
  }
}
