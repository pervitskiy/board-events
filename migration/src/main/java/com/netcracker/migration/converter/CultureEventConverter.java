package com.netcracker.migration.converter;

import com.netcracker.migration.db.models.NewEvent;
import com.netcracker.migration.jackson.models.NewEventDto;
import com.netcracker.migration.services.EventsService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CultureEventConverter {

  private final EventsService service;

  public CultureEventConverter() {
    service = new EventsService();
  }

  public NewEvent convert(NewEventDto response){
    NewEvent event = new NewEvent();
    event.setCultureId(response.getId());
    event.setVkId(0);

    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    LocalDateTime date = LocalDateTime.parse(
            response.getData().getGeneral().getStartTime(),
            inputFormatter
    );

    event.setDateTime(date);
    if (event.getDateTime().isBefore(LocalDateTime.now()))
      return event;
    event.setCountry("Россия");
    event.setCityId(
            service.searchCityId(
                    response.getData()
                    .getGeneral()
                    .getOrganization()
                    .getAddress()
                    .getName(),
                    event.getCountry()
                    )
    );

    event.setImageURL(response.getData().getGeneral().getImage().getUrl());
    event.setAgeRestriction(response.getData().getGeneral().getAge());
    event.setBriefDescription(response.getData().getGeneral().getName());
    event.setFullDescription(response.getData().getGeneral().getDescription());
    event.setPlace(response.getData().getGeneral().getPlacesList().get(0).getAddress().getFullAddress());

    return event;
  }
}
