package com.netcracker.migration.converter;

import com.netcracker.migration.db.models.NewEvent;
import com.netcracker.migration.services.EventsService;
import com.vk.api.sdk.objects.groups.responses.GetByIdLegacyResponse;
import org.springframework.web.util.UrlPathHelper;

import javax.print.attribute.URISyntax;
import java.net.URI;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class VkEventConverter {

	private final EventsService service;

	public VkEventConverter() {
		service = new EventsService();
	}

	public NewEvent convert(GetByIdLegacyResponse response, Integer lastCityId, String lastCityName) {
		NewEvent event = new NewEvent();
		event.setVkId(response.getId());
		event.setCultureId(0);
		event.setBriefDescription(response.getName());
		try {
			if (!response.getCountry().getTitle().isBlank() || response.getCountry().getTitle() != null) {
				event.setCountry(response.getCountry().getTitle());
			}
		} catch (NullPointerException ex) {
			event.setCountry("undefined");
		}
		try {
			switch (response.getAgeLimits().ordinal()) {
				case 1 -> event.setAgeRestriction(0);
				case 2 -> event.setAgeRestriction(16);
				case 3 -> event.setAgeRestriction(18);
				default -> event.setAgeRestriction(null);
			}
		} catch (NullPointerException ex) {
			event.setAgeRestriction(null);
		}

		try {
			if (response.getCity().getTitle() != null) {
				if (response.getCity().getTitle().equals(lastCityName)) {
					event.setCityId(lastCityId);
				} else {
					event.setCityId(
									service.searchCityId(
													response.getCity().getTitle(),
													event.getCountry()
									)
					);
				}
			}
		} catch (NullPointerException ex) {
			event.setCityId(3);
		}
		event.setFullDescription(response.getDescription());
		event.setImageURL(response.getPhoto200().getHost() + response.getPhoto200().getPath() + "?" + response.getPhoto200().getQuery());
		System.out.println(event.getImageURL());
		try {
			event.setDateTime(LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(response.getStartDate().toString())),
							TimeZone.getDefault().toZoneId()));
		} catch (NullPointerException ex) {
			event.setDateTime(LocalDateTime.MIN);
		}
		return event;
	}

}
