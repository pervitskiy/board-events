package com.netcracker.migration.db;

import java.util.List;

public interface DAO<T> {

	boolean add(T entity);
}
