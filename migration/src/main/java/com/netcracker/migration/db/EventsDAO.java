package com.netcracker.migration.db;

import com.netcracker.migration.db.models.ExistingEvent;
import com.netcracker.migration.db.models.NewEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.UUID;

public class EventsDAO implements DAO<NewEvent> {

	static final Logger logger = LoggerFactory.getLogger(EventsDAO.class);

	private final Connection connection = Connect.connect();

	@Override
	public boolean add(NewEvent entity) {
		try{
			PreparedStatement statement = connection.prepareStatement(
							"insert into \"events\"(" +
											"brief_description," +
											"date_time," +
											"full_description," +
											"place," +
											"cities_city_id," +
											"creation_date," +
											"vk_id," +
											"culture_id," +
											"age_restriction," +
											"imageURL" +
											") " +
											"values (?,?,?,?,?,?,?,?,?,?)"
			);
			statement.setString(1,entity.getBriefDescription());
			statement.setTimestamp(2,Timestamp.valueOf(entity.getDateTime()));
			statement.setString(3, entity.getFullDescription());
			statement.setString(4,entity.getPlace());
			statement.setInt(5,entity.getCityId());
			statement.setTimestamp(6,Timestamp.valueOf(LocalDateTime.now()));
			statement.setInt(7,entity.getVkId());
			statement.setInt(8,entity.getCultureId());
			if (entity.getAgeRestriction() != null)
				statement.setString(9,String.valueOf(entity.getAgeRestriction()));
			else
				statement.setString(9,"undefined");
			if (!entity.getImageURL().isBlank() || entity.getImageURL() != null)
				statement.setString(10,entity.getImageURL());
			else
				statement.setString(10,"undefined");
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public int search(String cityName, String countryName){
		int city_id = 3,country_id = 236;
		if (countryName.equals("Россия"))
			country_id = 1;
		if (!countryName.equals("Россия")){
			try{
				PreparedStatement statement1 = connection.prepareStatement(
								"select * " +
								"from \"_countries\" " +
								"where \"title_ru\" = ?;"
				);
				statement1.setString(1,countryName);
				ResultSet resultSet1 = statement1.executeQuery();
				while(resultSet1.next()){
					country_id = resultSet1.getInt(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		try{
			logger.info(country_id + " " + cityName);
			if (cityName.contains("ё"))
				cityName = cityName.replace('ё','е');
			PreparedStatement statement = connection.prepareStatement(
							"select " +
											"city_id," +
											"country_id," +
											"important," +
											"region_id," +
											"title_ru," +
											"area_ru," +
											"region_ru " +
											"from \"_cities\" " +
											"where \"country_id\" = ? " +
											"and \"title_ru\" = ?;",
							ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY
			);
			statement.setInt(1,country_id);
			statement.setString(2,cityName);
			ResultSet resultSet = statement.executeQuery();
			int count = 0;
			while(resultSet.next()) {
				count++;
			}
			logger.info("result of town searching: " + count);
//			resultSet.absolute(1);
			resultSet.beforeFirst();
			if (count > 1) {
				while (resultSet.next()){
					if (resultSet.getBoolean(3) | resultSet.getString("area_ru") == null){
						city_id = resultSet.getInt(1);
						break;
					}
				}
			}
			else {
				while (resultSet.next())
					city_id = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return city_id;
  }

	public ExistingEvent getExistingEvent(Integer id, String type) {
		ExistingEvent existingEvent = new ExistingEvent();
		try{
			PreparedStatement statement1 = connection.prepareStatement(
							"select * from \"events\" where "+ type +" = ?;"
			);
			statement1.setInt(1,id);
			ResultSet resultSet = statement1.executeQuery();
			while (resultSet.next()){
				existingEvent.setCreationDate(resultSet.getTimestamp(8));
				existingEvent.setEventId((java.util.UUID) resultSet.getObject(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return existingEvent;
	}

	public boolean checkOnExist(Integer id,String type) {
		try{
			PreparedStatement statement1 = connection.prepareStatement(
							"select * from \"events\" where "+ type +" = ?;"
			);
			statement1.setInt(1,id);
			ResultSet resultSet = statement1.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(UUID id) {
		try{
			PreparedStatement statement1 = connection.prepareStatement(
							"delete from \"events\" where \"event_id\" = ?;"
			);
			statement1.setObject(1,id);
			statement1.executeQuery();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
