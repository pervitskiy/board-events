package com.netcracker.migration.db.models;

import java.sql.Timestamp;
import java.util.UUID;

public class ExistingEvent {

	private UUID eventId;

	private Timestamp creationDate;

	public UUID getEventId() {
		return eventId;
	}

	public void setEventId(UUID eventId) {
		this.eventId = eventId;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
}
