package com.netcracker.migration.db.models;

import java.time.LocalDateTime;

public class NewEvent {

	private LocalDateTime dateTime;

	private String place;

	private String briefDescription;

	private String fullDescription;

	private Integer cityId;

	private String country;

	private Integer ageRestriction;

	private String imageURL;

	private LocalDateTime creationDate;

	private Integer vkId;

	private Integer cultureId;

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getCultureId() {
		return cultureId;
	}

	public void setCultureId(Integer cultureId) {
		this.cultureId = cultureId;
	}

	public Integer getVkId() {
		return vkId;
	}

	public void setVkId(Integer vkId) {
		this.vkId = vkId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Integer getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(Integer ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getBriefDescription() {
		return briefDescription;
	}

	public void setBriefDescription(String briefDescription) {
		this.briefDescription = briefDescription;
	}

	public String getFullDescription() {
		return fullDescription;
	}

	public void setFullDescription(String fullDescription) {
		this.fullDescription = fullDescription;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
