package com.netcracker.migration.importers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.migration.converter.CultureEventConverter;
import com.netcracker.migration.db.models.ExistingEvent;
import com.netcracker.migration.db.models.NewEvent;
import com.netcracker.migration.jackson.models.NewEventDto;
import com.netcracker.migration.services.EventsService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CultureImporter {

	static final Logger logger = LoggerFactory.getLogger(CultureImporter.class);

	private final EventsService service;

	private List<NewEventDto> eventList;

	public CultureImporter() {
		service = new EventsService();
		eventList = new ArrayList<>();
	}

	public void downloadEvents(String version) {

		try {
			HttpClient client = HttpClient.newHttpClient();

			HttpRequest request = HttpRequest.newBuilder()
							.GET()
							.header("Accept", "application/json")
							//.header("Authorization", "Basic ci5raG9kemhhZXY6NDdiYdfjlmNUM=") if you need
							.uri(URI.create("https://opendata.mkrf.ru/opendata/7705851331-events/data-"+version+"-structure-2.zip"))
							.build();

			HttpResponse<InputStream> response = client.send(request, HttpResponse.BodyHandlers.ofInputStream());

			logger.info("received response");
			InputStream in = response.body();
			Files.copy(in, Paths.get("files/" + "zip"), StandardCopyOption.REPLACE_EXISTING);
			logger.info("file copied");
		}
    catch(IOException | InterruptedException ex){
				logger.error("No such file or version");
				ex.printStackTrace();
			}
		}

		public List<NewEventDto> parseEvents (ObjectMapper mapper){

//    CultureImporter app = new CultureImporter();

			try {

//      File jarFile = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
//      logger.info("jar file path is "+ jarFile.getPath());

				InputStream stream = getClass().getClassLoader().getResourceAsStream("files/80.json");
				File targetFile = new File("80.json");
				FileUtils.copyInputStreamToFile(stream, targetFile);

//      List<Path> result = app.getPathsFromResourceJAR("files");

//      for (Path path : result) {
//        System.out.println("Path : " + path);
//
//        String filePathInJAR = path.toString();
//
//        if (filePathInJAR.startsWith("/")) {
//          filePathInJAR = filePathInJAR.substring(1, filePathInJAR.length());
//        }
//
//        System.out.println("filePathInJAR : " + filePathInJAR);
//
//        InputStream is = app.getFileFromResourceAsStream(filePathInJAR);
//
//        printInputStream(is, mapper);
//
				eventList.addAll(mapper.readValue(
								targetFile,
								mapper.getTypeFactory().constructCollectionType(List.class, NewEventDto.class)
				));
//      }
			} catch (IOException e) {
				e.printStackTrace();
			}

			logger.info("size in parseEvents " + eventList.size());
			return eventList;
		}

		private void printInputStream (InputStream is, ObjectMapper mapper){
			try (InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
				eventList.addAll(mapper.readValue(
								streamReader,
								mapper.getTypeFactory().constructCollectionType(List.class, NewEventDto.class)
				));
			} catch (IOException e) {
				e.printStackTrace();
			}

			try (InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
					 BufferedReader reader = new BufferedReader(streamReader)) {

				String line;
				while ((line = reader.readLine()) != null) {
					System.out.println(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private List<Path> getPathsFromResourceJAR (String folder)
          throws URISyntaxException, IOException {

			List<Path> result;

			// get path of the current running JAR
			URI jarPath = CultureImporter.class.getProtectionDomain()
							.getCodeSource()
							.getLocation()
							.toURI();
			URL second = CultureImporter.class.getResource(CultureImporter.class.getSimpleName() + ".class");
			System.out.println("JAR Path :" + jarPath);
			System.out.println("JAR Path :" + second);

			// file walks JAR
//    URI uri = URI.create("jar:file:" + jarPath);
			URI uri = URI.create(String.valueOf(jarPath));
			try (FileSystem fs = FileSystems.newFileSystem(uri, Collections.emptyMap())) {
				result = Files.walk(fs.getPath(uri + File.separator + folder))
								.filter(Files::isRegularFile)
								.collect(Collectors.toList());
			}

			return result;
		}

		private InputStream getFileFromResourceAsStream (String fileName){

			ClassLoader classLoader = getClass().getClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(fileName);

			if (inputStream == null) {
				throw new IllegalArgumentException("file not found! " + fileName);
			} else {
				return inputStream;
			}

		}

		public void saveAllEvents (List < NewEventDto > eventList) {

			CultureEventConverter converter = new CultureEventConverter();
			for (NewEventDto eventDto : eventList) {
				if (service.checkOnExist(eventDto.getId(), "\"culture_id\"")) {
					ExistingEvent existingEvent = service.getExistingEvent(eventDto.getId(), "\"culture_id\"");
					DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
					LocalDateTime updatedDate = LocalDateTime.parse(
									eventDto.getData().getInfo().getUpdateDate(),
									inputFormatter
					);
					LocalDateTime creationDate = existingEvent.getCreationDate().toLocalDateTime();

					logger.info("creation date is : " + creationDate + "updated date is : " + updatedDate);
					if (creationDate.isBefore(updatedDate))
						service.delete(existingEvent.getEventId());
					else {
						continue;
					}
				}

				NewEvent newEvent = converter.convert(eventDto);
				if (newEvent.getDateTime().isAfter(LocalDateTime.now())) {
					service.add(newEvent);
					logger.info("event added");
				}
			}
		}
	}
