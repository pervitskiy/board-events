package com.netcracker.migration.importers;

import com.netcracker.migration.converter.VkEventConverter;
import com.netcracker.migration.db.models.NewEvent;
import com.netcracker.migration.services.EventsService;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.database.City;
import com.vk.api.sdk.objects.enums.GroupsType;
import com.vk.api.sdk.objects.groups.Address;
import com.vk.api.sdk.objects.groups.Fields;
import com.vk.api.sdk.objects.groups.Group;
import com.vk.api.sdk.objects.groups.responses.GetByIdLegacyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class VkImporter {

  static final Logger logger = LoggerFactory.getLogger(VkImporter.class);

  private final EventsService service;

  public VkImporter() {
    service = new EventsService();
  }

  public List<Integer> getCityIds(VkApiClient vk, UserActor actor, boolean needAll){
    List<Integer> mainCitiesIds = new ArrayList<>();
    for (int i = 1; i < 235; i++) {
      List<City> cities = new ArrayList<>();
      try {
        cities = vk.database().getCities(actor, i)
                .needAll(needAll)
                .execute()
                .getItems();
      } catch (ApiException | ClientException e) {
        e.printStackTrace();
      }
      for (City city : cities) {
        mainCitiesIds.add(city.getId());
      }
      logger.info("main cities id size: "+mainCitiesIds.size());
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return mainCitiesIds;
  }

  public List<Integer> getEventIds(List<Integer> mainCitiesIds,VkApiClient vk, UserActor actor){
    List<Integer> event_ids = new ArrayList<>();
    for (int i : mainCitiesIds) {
      List<Group> events = new ArrayList<>();
      try {
        events = vk.groups().search(actor, String.valueOf('d'))
                .cityId(i)
                .type(GroupsType.EVENT)
                .future(true)
                .offset(0)
                .count(1000)
                .execute()
                .getItems();
      } catch (ApiException | ClientException e) {
        e.printStackTrace();
      }
      for (Group group : events) {
        event_ids.add(group.getId());
        try {
          Thread.sleep(380);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      logger.info("event id's size: " + event_ids.size());
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    logger.debug("no errors here. event id's: " + event_ids.size());
    return event_ids;
  }

  public void saveAllEvents(List<Integer> eventIds,VkApiClient vk, UserActor actor) {

    VkEventConverter converter = new VkEventConverter();

    int offset, counter_id = eventIds.size();
    Integer lastCityId = 0;
    String lastCityName = "";

    //list of event groups
    List<GetByIdLegacyResponse> groupInfo = new ArrayList<>();
    while (counter_id > 0) {
      logger.info("current counter_id is: " + counter_id);
      offset = Math.min(counter_id, 500);
      try {
        groupInfo = vk.groups().getByIdLegacy(actor)
                .groupIds(eventIds.subList(counter_id - offset, counter_id).toString())
                .fields(
                        Fields.DESCRIPTION,
                        Fields.COUNTRY,
                        Fields.CITY,
                        Fields.START_DATE,
                        Fields.FINISH_DATE,
                        Fields.MEMBERS_COUNT,
                        Fields.ACTIVITY,
                        Fields.STATUS,
                        Fields.CONTACTS,
                        Fields.LINKS,
                        Fields.SITE,
                        Fields.COVER,
                        Fields.AGE_LIMITS
                )
                .execute();
      } catch (ApiException | ClientException | IllegalStateException e) {
        e.printStackTrace();
      }
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      for (GetByIdLegacyResponse response : groupInfo) {
        logger.debug("searching city for: " + response.getId());
        try {

          //check if event already exists in db
          if (service.checkOnExist(response.getId(),"\"vk_id\"")){
            logger.debug("already exists");
            continue;
          }

          List<Address> address = vk.groups().getAddresses(actor,response.getId())
                  .count(1)
                  .fields(
                          com.vk.api.sdk.objects.addresses.Fields.ADDRESS,
                          com.vk.api.sdk.objects.addresses.Fields.TITLE
                  )
                  .execute()
                  .getItems();

          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          //convert to entity and save
          NewEvent newEvent = converter.convert(response,lastCityId,lastCityName);

          if (newEvent.getDateTime().equals(LocalDateTime.MIN))
            continue;

          lastCityId = newEvent.getCityId();
          lastCityName = response.getCity().getTitle();

          if (address.size()!=0)
            newEvent.setPlace(address.get(0).getAddress());
          else
            newEvent.setPlace(response.getCity().getTitle());
          if (newEvent.getDateTime().isAfter(LocalDateTime.now()))
            service.add(newEvent);

        } catch (ApiException | ClientException e) {
          e.printStackTrace();
        }
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      counter_id = counter_id - 500;
    }
  }

}

