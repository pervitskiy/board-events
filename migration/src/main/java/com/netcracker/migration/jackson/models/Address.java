package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Address {
  
  //TODO add coordinates

  @JsonProperty("fullAddress")
  private String fullAddress;

  @JsonProperty("coordinates")
  private List<Double> coordinatesList;

  public String getFullAddress() {
    return fullAddress;
  }

  public void setFullAddress(String fullAddress) {
    this.fullAddress = fullAddress;
  }

  public List<Double> getCoordinatesList() {
    return coordinatesList;
  }

  public void setCoordinatesList(List<Double> coordinatesList) {
    this.coordinatesList = coordinatesList;
  }
}
