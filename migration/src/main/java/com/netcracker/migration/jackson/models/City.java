package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class City {

	@JsonProperty("name")
	private String name;

	@JsonProperty("timezone")
	private String timezone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
}
