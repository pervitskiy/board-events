package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {

  @JsonProperty("general")
  private General general;

  @JsonProperty("info")
  private Info info;

  public Info getInfo() {
    return info;
  }

  public void setInfo(Info info) {
    this.info = info;
  }

  public General getGeneral() {
    return general;
  }

  public void setGeneral(General general) {
    this.general = general;
  }
}
