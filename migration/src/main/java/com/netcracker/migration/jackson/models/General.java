package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class General {

  @JsonProperty("name")
  private String name;

  @JsonProperty("shortDescription")
  private String description;

  @JsonProperty("ageRestriction")
  private Integer age;

  @JsonProperty("start")
  private String startTime;

  @JsonProperty("isFree")
  private Boolean isFree;

  @JsonProperty("places")
  private List<Place> placesList;

  @JsonProperty("organization")
  private Organization organization;

  @JsonProperty("image")
  private Image image;

  @JsonProperty("seances")
  private List<Seance> seancesList;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public Boolean getFree() {
    return isFree;
  }

  public void setFree(Boolean free) {
    isFree = free;
  }

  public List<Place> getPlacesList() {
    return placesList;
  }

  public void setPlacesList(List<Place> placesList) {
    this.placesList = placesList;
  }

  public Image getImage() {
    return image;
  }

  public void setImage(Image image) {
    this.image = image;
  }

  public List<Seance> getSeancesList() {
    return seancesList;
  }

  public void setSeancesList(List<Seance> seancesList) {
    this.seancesList = seancesList;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }
}
