package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;

public class Image {

  @JsonProperty("url")
  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
