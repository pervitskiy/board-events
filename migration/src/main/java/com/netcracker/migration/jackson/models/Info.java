package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Info {

	@JsonProperty("updateDate")
	private String updateDate;

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
