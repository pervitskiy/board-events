package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.netcracker.migration.jackson.models.Data;

public class NewEventDto {

  @JsonProperty("nativeId")
  private Integer id;

  @JsonProperty("data")
  private Data data;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return getData().getGeneral().getName() + "\n" +
      getData().getGeneral().getDescription() + "\n" +
      getData().getGeneral().getAge() + "\n" +
      getData().getGeneral().getFree() + "\n" +
      getData().getGeneral().getPlacesList() + "\n" +
      getData().getGeneral().getSeancesList() + "\n";
  }
}
