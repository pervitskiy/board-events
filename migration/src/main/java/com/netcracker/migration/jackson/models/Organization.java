package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Organization {

	@JsonProperty("name")
	private String name;

	@JsonProperty("locale")
	private City address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public City getAddress() {
		return address;
	}

	public void setAddress(City address) {
		this.address = address;
	}
}
