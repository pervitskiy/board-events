package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Place {

  @JsonProperty("name")
  private String name;

  @JsonProperty("address")
  private Address address;

  public String getName() {
    return name;
  }

  public Address getAddress() {
    return address;
  }
}
