package com.netcracker.migration.jackson.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Seance {

  @JsonProperty("start")
  private String startDate;

  @JsonProperty("end")
  private String endDate;

}
