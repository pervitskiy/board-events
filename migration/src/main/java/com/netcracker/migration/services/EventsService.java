package com.netcracker.migration.services;

import com.netcracker.migration.db.EventsDAO;
import com.netcracker.migration.db.models.ExistingEvent;
import com.netcracker.migration.db.models.NewEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

@Component
public class EventsService {

  private final EventsDAO dao;

  @Autowired
  public EventsService() {
    dao = new EventsDAO();
  }

  public int searchCityId(String cityName, String countryName){
    return dao.search(cityName, countryName);
  }

  public void add(NewEvent event) {
    dao.add(event);
  }

  public ExistingEvent getExistingEvent(Integer id, String type){
    return dao.getExistingEvent(id,type);
  }

  public boolean checkOnExist(Integer id, String type){
    return dao.checkOnExist(id,type);
  }

  public boolean delete(UUID id) {
    return dao.delete(id);
  }
}
