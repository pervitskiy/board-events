create table if not exists like_user_on_event(
    like_user_on_event_id uuid primary key,
    events_event_id uuid REFERENCES Events(event_id),
    users_user_id uuid  REFERENCES Users(user_id)
);

create table if not exists favorites_user_on_event(
    favorites_user_on_event uuid primary key,
    events_event_id uuid REFERENCES Events(event_id),
    users_user_id uuid  REFERENCES Users(user_id)
);
