
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table if not exists categories(
    categories_id serial primary key,
    name_category varchar(100) not null UNIQUE
);

create table if not exists _countries(
       country_id integer unique not null,
       title_ru character varying(60),
       title_ua character varying(60),
       title_be character varying(60),
       title_en character varying(60),
       title_es character varying(60),
       title_pt character varying(60),
       title_de character varying(60),
       title_fr character varying(60),
       title_it character varying(60),
       title_pl character varying(60),
       title_ja character varying(60),
       title_lt character varying(60),
       title_lv character varying(60),
       title_cz character varying(60),
       country_uuid_id uuid default uuid_generate_v4() not null,
       CONSTRAINT pk_country_cid PRIMARY KEY (country_uuid_id)
);
CREATE SEQUENCE country_id_seq start with 1 INCREMENT BY 1;

create table if not exists _regions(
                         region_id integer  unique  NOT NULL,
                         region_uuid_id uuid default uuid_generate_v4() not null,
                         country_id integer REFERENCES _countries(country_id) on delete cascade,
                         title_ru character varying(150),
                         title_ua character varying(150),
                         title_be character varying(150),
                         title_en character varying(150),
                         title_es character varying(150),
                         title_pt character varying(150),
                         title_de character varying(150),
                         title_fr character varying(150),
                         title_it character varying(150),
                         title_pl character varying(150),
                         title_ja character varying(150),
                         title_lt character varying(150),
                         title_lv character varying(150),
                         title_cz character varying(150),
                         CONSTRAINT pk_region_rid PRIMARY KEY (region_uuid_id)
);
CREATE SEQUENCE regions_id_seq start with 1 INCREMENT BY 1;

create table if not exists _cities(
                        city_id integer unique NOT NULL,
                        country_id integer REFERENCES _countries(country_id),
                        city_uuid_id uuid default uuid_generate_v4() not null,
                        important boolean NOT NULL,
                        region_id integer REFERENCES _regions(region_id),

                        title_ru character varying(150),
                        area_ru character varying(150),
                        region_ru character varying(150),

                        title_ua character varying(150),
                        area_ua character varying(150),
                        region_ua character varying(150),

                        title_be character varying(150),
                        area_be character varying(150),
                        region_be character varying(150),

                        title_en character varying(150),
                        area_en character varying(150),
                        region_en character varying(150),

                        title_es character varying(150),
                        area_es character varying(150),
                        region_es character varying(150),

                        title_pt character varying(150),
                        area_pt character varying(150),
                        region_pt character varying(150),

                        title_de character varying(150),
                        area_de character varying(150),
                        region_de character varying(150),

                        title_fr character varying(150),
                        area_fr character varying(150),
                        region_fr character varying(150),

                        title_it character varying(150),
                        area_it character varying(150),
                        region_it character varying(150),

                        title_pl character varying(150),
                        area_pl character varying(150),
                        region_pl character varying(150),

                        title_ja character varying(150),
                        area_ja character varying(150),
                        region_ja character varying(150),

                        title_lt character varying(150),
                        area_lt character varying(150),
                        region_lt character varying(150),

                        title_lv character varying(150),
                        area_lv character varying(150),
                        region_lv character varying(150),

                        title_cz character varying(150),
                        area_cz character varying(150),
                        region_cz character varying(150),

                        CONSTRAINT pk_city_cid PRIMARY KEY (city_uuid_id)
);

CREATE SEQUENCE city_id_seq start with 1 INCREMENT BY 1;


create table if not exists events(
                       event_id uuid primary key default uuid_generate_v4(),
                       date_time timestamp with time zone not null,
                       place varchar(255) not null,
                       brief_description varchar(500) not null,
                       full_description text not null,
                       external_event varchar(255),
                       cities_city_id INTEGER REFERENCES _cities(city_id)
);

create table if not exists events_has_categories(
    events_has_categories_id uuid primary key,
    events_event_id uuid REFERENCES Events(event_id),
    categories_categories_id INTEGER REFERENCES categories(categories_id)
);

create table if not exists users(
    user_id uuid primary key,
    first_name varchar(100) not null,
    last_name varchar(100) not null,
    information_about_yourself text,
    email varchar(100) not null UNIQUE CHECK(email !=''),
    password varchar(30) not null
);

create table if not exists organizers_has_events(
  organizers_has_event_id uuid primary key,
  events_event_id uuid  REFERENCES Events(event_id),
  users_user_id uuid REFERENCES Users(user_id)
);

create table if not exists users_has_events(
  users_has_event_id uuid primary key,
  events_event_id uuid REFERENCES Events(event_id),
  users_user_id uuid  REFERENCES Users(user_id)
);

create table if not exists rating(
  id_rating uuid primary key,
  val_rating real  not null,
  events_event_id uuid REFERENCES Events(event_id),
  users_user_id uuid  REFERENCES Users(user_id)
);


create table if not exists categories_file(
    categories_file_id serial primary key,
    categories_file_name varchar(100) not null
);


create table if not exists media(
    media_id uuid primary key,
    file_name varchar(100) not null unique,
    categories_file_categories_file_id INTEGER REFERENCES categories_file(categories_file_id),
    users_user_id uuid REFERENCES Users(user_id) on delete cascade,
    events_event_id uuid REFERENCES Events(event_id)
);

create table if not exists payment_information(
    payment_information_id uuid primary key,
    dateTime timestamp with time zone not null,
    status varchar(30) not null,
    events_event_id uuid  REFERENCES Events(event_id),
    users_user_id uuid REFERENCES Users(user_id)
);

create table if not exists comments(
    comments_id uuid primary key,
    message text not null,
    comments_date timestamp with time zone not null,
    events_event_id uuid REFERENCES Events(event_id),
    users_user_id uuid REFERENCES Users(user_id)
);

create table if not exists categories_has_users(
  categories_has_users_id uuid primary key,
  categories_categories_id INTEGER REFERENCES categories(categories_id),
  users_user_id uuid  REFERENCES Users(user_id)
);
