ALTER TABLE events
ADD rating double precision;

ALTER TABLE events
ADD members_count integer;
