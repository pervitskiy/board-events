ALTER TABLE events
    alter column rating set default 0;

ALTER TABLE events
    alter column members_count set default 1;
